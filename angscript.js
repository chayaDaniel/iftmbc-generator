function AppData() {
	// body...
	this.name = "IFTMBC Generator";
	this.version = "1.0";
	this.settings = {
		senders : [
				  {
					"sendername": "HAPAG LLOYD",
					"sendercode": "HAPAG-LLOYD",
				  },
				  {
					"sendername": "TRAXENS",
					"sendercode": "TRAXENS"
				  },
				  {
					"sendername": "DAHER",
					"sendercode": "DAHER"
				  },
				  {
					"sendername": "CIMC",
					"sendercode": "CIMC"
				  },
				  {
					"sendername": "CMA-CGM",
					"sendercode": "CMDU"
				  },
				  {
					"sendername": "ANL",
					"sendercode": "ANNU"
				  },
				  {
					"sendername": "CNC",
					"sendercode": "CHNL"
				  },
				  {
					"sendername": "MSC",
					"sendercode": "MSCU"
				  },
				  {
					"sendername": "MAERSK",
					"sendercode": "MAEU"
				  }
				],
recipients : [
				  {
					"sendername": "HAPAG LLOYD",
					"sendercode": "HAPAG-LLOYD",
				  },
				  {
					"sendername": "TRAXENS",
					"sendercode": "TRAXENS"
				  },
				  {
					"sendername": "DAHER",
					"sendercode": "DAHER"
				  },
				  {
					"sendername": "CIMC",
					"sendercode": "CIMC"
				  },
				  {
					"sendername": "CMA-CGM",
					"sendercode": "CMDU"
				  },
				  {
					"sendername": "ANL",
					"sendercode": "ANNU"
				  },
				  {
					"sendername": "CNC",
					"sendercode": "CHNL"
				  },
				  {
					"sendername": "MSC",
					"sendercode": "MSCU"
				  },
				  {
					"sendername": "MAERSK",
					"sendercode": "MAEU"
				  }
				],
		carriers: [
				  {
					"carriername": "HAPAG LLOYD",
					"carriercode": "HAPAG-LLOYD"
				  },

				  {
					"carriername": "DAHER",
					"carriercode": "DAHER"
				  },
				  {
					"carriername": "CIMC",
					"carriercode": "CIMC"
				  },
				  {
					"carriername": "CMA-CGM",
					"carriercode": "CMDU"
				  },
				  {
					"carriername": "ANL",
					"carriercode": "ANNU"
				  },
				  {
					"carriername": "CNC",
					"carriercode": "CHNL"
				  },
				  {
					"carriername": "MSC",
					"carriercode": "MSCU"
				  },
				  {
					"carriername": "MAERSK",
					"carriercode": "MAEU"
				  }
				],

		cli : [
			{
				"Bookingpartners": "ACME",
				"Partnercode": "91625"
			},
			{
				"Bookingpartners": "LIDL",
				"Partnercode": "DE102571"
			},
			{
				"Bookingpartners": "ACME",
				"Partnercode": "334455"
			},
			{
				"Bookingpartners": "SAFO - SOGEDIAL EXPLOITATION",
				"Partnercode": "11313"
			},
			{
				"Bookingpartners": "DECATHLON",
				"Partnercode": "44531"
			},
			{
				"Bookingpartners": "CMA CGM LOGISTICS (CC LOG)",
				"Partnercode": "1165761"
			},
			{
				"Bookingpartners": "COMBIPASS",
				"Partnercode": ""
			},
			{
				"Bookingpartners": "T3M",
				"Partnercode": ""
			},
			{
				"Bookingpartners": "DECATHLON",
				"Partnercode": "44531"
			},
			{
				"Bookingpartners": "TESLA",
				"Partnercode": "US650521"
			},
			{
				"Bookingpartners": "ACME",
				"Partnercode": "334455"
			},
			{
				"Bookingpartners": "ERMEWA",
				"Partnercode": ""
			},
			{
				"Bookingpartners": "HAPAG LLOYD",
				"Partnercode": ""
			},
			{
				"Bookingpartners": "DAHER",
				"Partnercode": ""
			},
			{
				"Bookingpartners": "SNCF",
				"Partnercode": ""
			},
			{
				"Bookingpartners": "CIMC",
				"Partnercode": ""
			},
			{
				"Bookingpartners": "MAERSK",
				"Partnercode": ""
			},
			{
				"Bookingpartners": ""
			}
		],

		EQP: [
        {
            "Typecode": "42T2",
            "Description": "Tank container - Non dangerous liquids, minimum pressure 2.65 bar"
        },
        {
            "Typecode": "22T7",
            "Description": "Tank container - Gases, minimum pressure 9.10 bar"
        },
        {
            "Typecode": "20T4",
            "Description": "Tank container - Dangerous liquids, minimum pressure 2.65 bar"
        },
        {
            "Typecode": "45U1",
            "Description": "Open Top - Openings at one or both ends and removable top members in end frames"
        },
        {
            "Typecode": "45G1",
            "Description": "High-Cube General purpose container - Passive vents at upper part of cargo space"
        },
        {
            "Typecode": "22P9",
            "Description": "Flat transport unit (collapsible)"
        },
        {
            "Typecode": "46H0",
            "Description": "Refrigerated or heated container with removable equipment located externally"
        },
        {
            "Typecode": "22T6",
            "Description": "Tank container - Dangerous liquids, minimum pressure 6.00 bar"
        },
        {
            "Typecode": "20T0",
            "Description": "Tank container - Non dangerous liquids, minimum pressure 0.45 bar"
        },
        {
            "Typecode": "22U6",
            "Description": "Hard-Top container"
        },
        {
            "Typecode": "48T8",
            "Description": "Tank container - Gases, minimum pressure 22.00 bar"
        },
        {
            "Typecode": "22P3",
            "Description": "Flat or Bolster - Folding complete end structure (collapsible)"
        },
        {
            "Typecode": "20T5",
            "Description": "Tank container - Dangerous liquids, minimum pressure 4.00 bar"
        },
        {
            "Typecode": "45B3",
            "Description": "Bulk container - Horizontal discharge, test pressure 1.50 bar"
        },
        {
            "Typecode": "L0G1",
            "Description": "High-Cube General purpose container - Passive vents at upper part of cargo space"
        },
        {
            "Typecode": "42H0",
            "Description": "Refrigerated or heated container with removable equipment located externally"
        },
        {
            "Typecode": "45P3",
            "Description": "Flat or Bolster - Folding complete end structure (collapsible)"
        },
        {
            "Typecode": "22P1",
            "Description": "Flat or Bolster - Two complete and fixed ends"
        },
        {
            "Typecode": "22G0",
            "Description": "General purpose container - Openings at one or both ends"
        },
        {
            "Typecode": "42P1",
            "Description": "Flat or Bolster - Two complete and fixed ends"
        },
        {
            "Typecode": "42R9",
            "Description": "Integral Reefer container - No food allowed"
        },
        {
            "Typecode": "22V4",
            "Description": "Fantainer - Mechanical ventilation system located externally"
        },
        {
            "Typecode": "22V0",
            "Description": "Fantainer - Non-mechanical, vents at lower and upper parts of cargo space"
        },
        {
            "Typecode": "22R7",
            "Description": "Integral Reefer with Built-in  power supply generator"
        },
        {
            "Typecode": "42P6",
            "Description": "Flat transport unit"
        },
        {
            "Typecode": "20RF",
            "Description": "20x8 6,Mechanically refrigerated and heated"
        },
        {
            "Typecode": "22T3",
            "Description": "Tank container - Dangerous liquids, minimum pressure 1.50 bar"
        },
        {
            "Typecode": "45R9",
            "Description": "Integral Reefer container - No food allowed"
        },
        {
            "Typecode": "26H0",
            "Description": "Refrigerated or heated container with removable equipment located externally"
        },
        {
            "Typecode": "42S1",
            "Description": "Live fish carrier"
        },
        {
            "Typecode": "45R1",
            "Description": "Integral Reefer - Mechanically refrigerated and heated"
        },
        {
            "Typecode": "4CG0",
            "Description": "General purpose container - Openings at one or both ends"
        },
        {
            "Typecode": "42U6",
            "Description": "Hard-Top container"
        },
        {
            "Typecode": "42P9",
            "Description": "Flat transport unit (collapsible)"
        },
        {
            "Typecode": "22B0",
            "Description": "Bulk container - Closed"
        },
        {
            "Typecode": "42P3",
            "Description": "Flat or Bolster - Folding complete end structure (collapsible)"
        },
        {
            "Typecode": "LNR1",
            "Description": "High-Cube Integral Reefer - Mechanically refrigerated and heated"
        },
        {
            "Typecode": "26G0",
            "Description": "General purpose container - Openings at one or both ends"
        },
        {
            "Typecode": "22T5",
            "Description": "Tank container - Dangerous liquids, minimum pressure 4.00 bar"
        },
        {
            "Typecode": "42G1",
            "Description": "General purpose container - Passive vents at upper part of cargo space"
        },
        {
            "Typecode": "22S1",
            "Description": "Automobile carrier"
        },
        {
            "Typecode": "42U1",
            "Description": "Open Top - Openings at one or both ends and removable top members in end frames"
        },
        {
            "Typecode": "42R3",
            "Description": "Integral Reefer with diesel generator"
        },
        {
            "Typecode": "28T8",
            "Description": "Tank container - Gases, minimum pressure 22.00 bar"
        },
        {
            "Typecode": "22T1",
            "Description": "Tank container - Non dangerous liquids, minimum pressure 1.50 bar"
        },
        {
            "Typecode": "22H0",
            "Description": "Refrigerated or heated container with removable equipment located externally"
        },
        {
            "Typecode": "20H0",
            "Description": "Refrigerated or heated container with removable equipment located externally"
        },
        {
            "Typecode": "20T2",
            "Description": "Tank container - Non dangerous liquids, minimum pressure 2.65 bar"
        },
        {
            "Typecode": "22P7",
            "Description": "Flat transport unit"
        },
        {
            "Typecode": "22U1",
            "Description": "Open Top - Openings at one or both ends and removable top members in end frames"
        },
        {
            "Typecode": "45P8",
            "Description": "Flat transport unit (collapsible flush folding)"
        },
        {
            "Typecode": "42T6",
            "Description": "Tank container - Dangerous liquids, minimum pressure 6.00 bar"
        },
        {
            "Typecode": "20G1",
            "Description": "General purpose container - Passive vents at upper part of cargo space"
        },
        {
            "Typecode": "42T8",
            "Description": "Tank container - Gases, minimum pressure 22.00 bar"
        },
        {
            "Typecode": "28U1",
            "Description": "Open Top - Openings at one or both ends and removable top members in end frames"
        },
        {
            "Typecode": "42P8",
            "Description": "Flat transport unit (collapsible flush folding)"
        },
        {
            "Typecode": "22V2",
            "Description": "Fantainer - Mechanical ventilation system located internally"
        },
        {
            "Typecode": "22S0",
            "Description": "Livestock carrier"
        },
        {
            "Typecode": "45U6",
            "Description": "Hard-Top container"
        },
        {
            "Typecode": "20G0",
            "Description": "General purpose container - Openings at one or both ends"
        },
        {
            "Typecode": "44R1",
            "Description": "Integral Reefer - Mechanically refrigerated and heated"
        },
        {
            "Typecode": "20P1",
            "Description": "Flat or Bolster - Two complete and fixed ends"
        },
        {
            "Typecode": "42T5",
            "Description": "Tank container - Dangerous liquids, minimum pressure 4.00 bar"
        },
        {
            "Typecode": "44V2",
            "Description": "Fantainer - Mechanical ventilation system located internally"
        },
        {
            "Typecode": "28V0",
            "Description": "Fantainer - Non-mechanical, vents at lower and upper parts of cargo space"
        },
        {
            "Typecode": "40RP",
            "Description": "40x9 6 Reefer High Cube Primeline"
        },
        {
            "Typecode": "22P8",
            "Description": "Flat transport unit (collapsible flush folding)"
        },
        {
            "Typecode": "22T4",
            "Description": "Tank container - Dangerous liquids, minimum pressure 2.65 bar"
        },
        {
            "Typecode": "20T8",
            "Description": "Tank container - Gases, minimum pressure 22.00 bar"
        },
        {
            "Typecode": "20T6",
            "Description": "Tank container - Dangerous liquids, minimum pressure 6.00 bar"
        },
        {
            "Typecode": "29P0",
            "Description": "Flat or Bolster - Plain platform"
        },
        {
            "Typecode": "22T8",
            "Description": "Tank container - Gases, minimum pressure 22.00 bar"
        },
        {
            "Typecode": "42R1",
            "Description": "Integral Reefer - Mechanically refrigerated and heated"
        },
        {
            "Typecode": "49P0",
            "Description": "Platform - Flat or Bolster - Plain platform"
        },
        {
            "Typecode": "20T3",
            "Description": "Tank container - Dangerous liquids, minimum pressure 1.50 bar"
        },
        {
            "Typecode": "45G0",
            "Description": "General purpose container - Openings at one or both ends"
        },
        {
            "Typecode": "22R1",
            "Description": "Integral Reefer - Mechanically refrigerated and heated"
        },
        {
            "Typecode": "25G0",
            "Description": "General purpose container - Openings at one or both ends"
        },
        {
            "Typecode": "L2G1",
            "Description": "High-Cube General purpose container - Passive vents at upper part of cargo space"
        },
        {
            "Typecode": "22R9",
            "Description": "Integral Reefer container - No food allowed"
        },
        {
            "Typecode": "2EG0",
            "Description": "General purpose container - Openings at one or both ends"
        },
        {
            "Typecode": "42G0",
            "Description": "General purpose container - Openings at one or both ends"
        },
        {
            "Typecode": "20T1",
            "Description": "Tank container - Non dangerous liquids, minimum pressure 1.50 bar"
        },
        {
            "Typecode": "20T7",
            "Description": "Tank container - Gases, minimum pressure 9.10 bar"
        },
        {
            "Typecode": "22T2",
            "Description": "Tank container - Non dangerous liquids, minimum pressure 2.65 bar"
        },
        {
            "Typecode": "22G1",
            "Description": "General purpose container - Passive vents at upper part of cargo space"
        }
    ],

    countries: [
	  {
	    "Code": "TNTUN",
	    "Nom": "Tunis"
	  },
	  {
	    "Code": "MYPKG",
	    "Nom": "Pelabuhan Klang"
	  },
	  {
	    "Code": "SITEPUBLICC",
	    "Nom": "Site Public C (créé par CMA)"
	  },
	  {
	    "Code": "AESHJ",
	    "Nom": "SHARJAH"
	  },
	  {
	    "Code": "USBOS",
	    "Nom": "Boston"
	  },
	  {
	    "Code": "USACI",
	    "Nom": "Tacoma"
	  },
	  {
	    "Code": "FRSXB",
	    "Nom": "Strasbourg"
	  },
	  {
	    "Code": "VCCRP",
	    "Nom": "Huntington"
	  },
	  {
	    "Code": "CAHAL",
	    "Nom": "Halifax"
	  },
	  {
	    "Code": "USBAL",
	    "Nom": "Baltimore"
	  },
	  {
	    "Code": "ESALG",
	    "Nom": "Algeciras"
	  },
	  {
	    "Code": "USASF",
	    "Nom": "Ashtabula"
	  },
	  {
	    "Code": "GFDDC",
	    "Nom": "Degrad-Des Cannes"
	  },
	  {
	    "Code": "AEAJM",
	    "Nom": "Ajman"
	  },
	  {
	    "Code": "FRURO",
	    "Nom": "Rouen"
	  },
	  {
	    "Code": "EGPSE",
	    "Nom": "Port Said East"
	  },
	  {
	    "Code": "FRLEH",
	    "Nom": "Le Havre"
	  },
	  {
	    "Code": "FRBOL",
	    "Nom": "Boulogne Sur Mer"
	  },
	  {
	    "Code": "FRBOD",
	    "Nom": "Bordeaux"
	  },
	  {
	    "Code": "FRTLN",
	    "Nom": "Toulon"
	  },
	  {
	    "Code": "FRNTE",
	    "Nom": "Nantes-St. Nazaire"
	  },
	  {
	    "Code": "SITEPUBLICD",
	    "Nom": "Site Public D (créé par CMA)"
	  },
	  {
	    "Code": "FRCER",
	    "Nom": "Cherbourg"
	  },
	  {
	    "Code": "SITEPUBLICB",
	    "Nom": "Site Public B (créé par CMA)"
	  },
	  {
	    "Code": "FRFOS",
	    "Nom": "Fos sur Mer"
	  },
	  {
	    "Code": "FRAIS",
	    "Nom": "Cassis"
	  },
	  {
	    "Code": "IDJKT",
	    "Nom": "JAKARTA"
	  },
	  {
	    "Code": "USAOT",
	    "Nom": "Anacortes"
	  },
	  {
	    "Code": "FRLPE",
	    "Nom": "La Rochelle-Pallice"
	  },
	  {
	    "Code": "FRPAR",
	    "Nom": "Paris"
	  },
	  {
	    "Code": "LRUCN",
	    "Nom": "Buchanan"
	  },
	  {
	    "Code": "YTLON",
	    "Nom": "Longoni"
	  },
	  {
	    "Code": "OMSLL",
	    "Nom": "UNKNOWN"
	  },
	  {
	    "Code": "MQLMT",
	    "Nom": "Le Lamentin"
	  },
	  {
	    "Code": "FRLYO",
	    "Nom": "Lyon"
	  },
	  {
	    "Code": "GBGRK",
	    "Nom": "Greenock"
	  },
	  {
	    "Code": "FRDKK",
	    "Nom": "Dunkerque"
	  },
	  {
	    "Code": "FRSML",
	    "Nom": "St Malo"
	  },
	  {
	    "Code": "FRMRS",
	    "Nom": "Marseille"
	  },
	  {
	    "Code": "GBTIL",
	    "Nom": "Tilbury"
	  },
	  {
	    "Code": "NZLYT",
	    "Nom": "Lyttelton"
	  },
	  {
	    "Code": "CNYTN",
	    "Nom": "Shenzhen"
	  },
	  {
	    "Code": "FRSCA",
	    "Nom": "Site Calendal"
	  },
	  {
	    "Code": "FRSTR",
	    "Nom": "Site Traxens"
	  },
	  {
	    "Code": "MQFDF",
	    "Nom": "Fort de France"
	  }
	],

	portNC: [

	{
		"Pname": "ABU DHABI",
		"Pcode": "AEAUH"
	},
	{
		"Pname": "ABU DHABI KHALIFA",
		"Pcode": "AEKHL"
	},
	{
		"Pname": "AL FUJAIRAH",
		"Pcode": "AEFJR"
	},
	{
		"Pname": "DUBAI",
		"Pcode": "AEDXB"
	},
	{
		"Pname": "JEBELALI",
		"Pcode": "AEJE4"
	},
	{
		"Pname": "JEBELALI",
		"Pcode": "AEJE3"
	},
	{
		"Pname": "JEBELALI",
		"Pcode": "AEJEA"
	},
	{
		"Pname": "JEBELALI",
		"Pcode": "AEJAL"
	},
	{
		"Pname": "JEBELALI",
		"Pcode": "AEJE2"
	},
	{
		"Pname": "JEBELALI",
		"Pcode": "AEJE1"
	},
	{
		"Pname": "KHOR AL FAKKAN",
		"Pcode": "AEKLF"
	},
	{
		"Pname": "MINA QABOOS",
		"Pcode": "AEMQA"
	},
	{
		"Pname": "SHARJAH",
		"Pcode": "AESHJ"
	},

	{
		"Pname": "ARUBA",
		"Pcode": "ANAUA"
	},
	{
		"Pname": "CURACAO",
		"Pcode": "ANCUR"
	},
	{
		"Pname": "LOBITO",
		"Pcode": "AOLOB"
	},
	{
		"Pname": "LUANDA",
		"Pcode": "AOLAD"
	},
	{
		"Pname": "BUENOS AIRES",
		"Pcode": "ARBUE"
	},
	{
		"Pname": "PAGO PAGO",
		"Pcode": "ASPPG"
	},
	{
		"Pname": "ADELAIDE",
		"Pcode": "AUADL"
	},
	{
		"Pname": "ALBANY",
		"Pcode": "AUALH"
	},
	{
		"Pname": "BELLBAY",
		"Pcode": "AUBEL"
	},
	{
		"Pname": "BRISBANE",
		"Pcode": "AUBNE"
	},
	{
		"Pname": "BROOME",
		"Pcode": "AUBME"
	},
	{
		"Pname": "BURNIE",
		"Pcode": "AUBUR"
	},
	{
		"Pname": "CAIRNS",
		"Pcode": "AUCNS"
	},
	{
		"Pname": "DARWIN",
		"Pcode": "AUDRW"
	},
	{
		"Pname": "DEVONPORT",
		"Pcode": "AUDPO"
	},
	{
		"Pname": "FREMANTLE",
		"Pcode": "AUFRE"
	},
	{
		"Pname": "GEELONG",
		"Pcode": "AUGEX"
	},
	{
		"Pname": "HOBART",
		"Pcode": "AUHBA"
	},
	{
		"Pname": "LAUNCESTON",
		"Pcode": "AULST"
	},
	{
		"Pname": "MELBOURNE",
		"Pcode": "AUMEL"
	},
	{
		"Pname": "NEWCASTLE",
		"Pcode": "AUNTL"
	},
	{
		"Pname": "PORT HEDLAND",
		"Pcode": "AUPHE"
	},
	{
		"Pname": "SYDNEY",
		"Pcode": "AUSYD"
	},
	{
		"Pname": "TOWNSVILLE",
		"Pcode": "AUTSV"
	},
	{
		"Pname": "BAHRAIN",
		"Pcode": "BAKBS"
	},
	{
		"Pname": "BAHRAIN",
		"Pcode": "BABAH"
	},
	{
		"Pname": "BRIDGETOWN",
		"Pcode": "BBBGI"
	},
	{
		"Pname": "CHALNA",
		"Pcode": "BDCHL"
	},
	{
		"Pname": "CHITTAGONG",
		"Pcode": "BDCGP"
	},
	{
		"Pname": "MONGLA/CHALNA",
		"Pcode": "BDMON"
	},
	{
		"Pname": "ANTWERPEN",
		"Pcode": "BEANR"
	},
	{
		"Pname": "GENT",
		"Pcode": "BEGNE"
	},
	{
		"Pname": "ZEEBRUGGE",
		"Pcode": "BEZEE"
	},
	{
		"Pname": "VARNA",
		"Pcode": "BGVAR"
	},
	{
		"Pname": "BAHRAIN",
		"Pcode": "BHBAH"
	},
	{
		"Pname": "COTONOU",
		"Pcode": "BJCOO"
	},
	{
		"Pname": "BRUNEI TOWN",
		"Pcode": "BNBTN"
	},
	{
		"Pname": "SERIA",
		"Pcode": "BNSER"
	},
	{
		"Pname": "BRRSH",
		"Pcode": "BRRSH"
	},
	{
		"Pname": "FORTALEZA",
		"Pcode": "BRFOR"
	},
	{
		"Pname": "ITAGUAI",
		"Pcode": "BRIGI"
	},
	{
		"Pname": "ITAJAI",
		"Pcode": "BRITJ"
	},
	{
		"Pname": "ITAQUI",
		"Pcode": "BRITQ"
	},
	{
		"Pname": "MANAUS",
		"Pcode": "BRMAO"
	},
	{
		"Pname": "NAVEGANTES",
		"Pcode": "BRNVT"
	},
	{
		"Pname": "PARANAGUA",
		"Pcode": "BRPNG"
	},
	{
		"Pname": "PECEM",
		"Pcode": "BRPEC"
	},
	{
		"Pname": "PORTO ALEGRE",
		"Pcode": "BRPOA"
	},
	{
		"Pname": "RECIFE",
		"Pcode": "BRREC"
	},
	{
		"Pname": "RIO DE JANEIRO",
		"Pcode": "BRRIO"
	},
	{
		"Pname": "RIO GRANDE",
		"Pcode": "BRRIG"
	},
	{
		"Pname": "SALVADOR",
		"Pcode": "BRSSA"
	},
	{
		"Pname": "SANTOS",
		"Pcode": "BRSSZ"
	},
	{
		"Pname": "SAO FRANCISCO",
		"Pcode": "BRQFS"
	},
	{
		"Pname": "SEPETIBA",
		"Pcode": "BRSPB"
	},
	{
		"Pname": "SUAPE",
		"Pcode": "BRSUA"
	},
	{
		"Pname": "VITORIA",
		"Pcode": "BRVIX"
	},

	{
		"Pname": "FREEPORT",
		"Pcode": "BSFPO"
	},
	{
		"Pname": "NASSAU",
		"Pcode": "BSNAS"
	},
	{
		"Pname": "BELIZE CITY",
		"Pcode": "BZBZE"
	},
	{
		"Pname": "EDMONTON",
		"Pcode": "CAEDM"
	},
	{
		"Pname": "HALIFAX",
		"Pcode": "CAHAL"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "CALMN"
	},
	{
		"Pname": "MONTRAL",
		"Pcode": "CAMTL"
	},
	{
		"Pname": "MONTREAL",
		"Pcode": "CAMTR"
	},
	{
		"Pname": "PORTLAND",
		"Pcode": "CAPT1"
	},
	{
		"Pname": "QUEBEC",
		"Pcode": "CAQUE"
	},
	{
		"Pname": "ST JOHNS",
		"Pcode": "CASJE"
	},
	{
		"Pname": "TORONTO",
		"Pcode": "CATOR"
	},
	{
		"Pname": "VANCOUVER",
		"Pcode": "CAVA1"
	},
	{
		"Pname": "VANCOUVER",
		"Pcode": "CAVAN"
	},
	{
		"Pname": "VANCOUVER",
		"Pcode": "CAVCP"
	},
	{
		"Pname": "VANCOUVER",
		"Pcode": "CAVA4"
	},
	{
		"Pname": "VANCOUVER",
		"Pcode": "CAVA3"
	},
	{
		"Pname": "VANCOUVER",
		"Pcode": "CAYV1"
	},
	{
		"Pname": "VANCOUVER",
		"Pcode": "CAVA2"
	},
	{
		"Pname": "VANCOUVER 1",
		"Pcode": "CAYCR"
	},
	{
		"Pname": "VANCOUVER 2",
		"Pcode": "CAKCR"
	},
	{
		"Pname": "VANCOUVER 3",
		"Pcode": "CAVCR"
	},
	{
		"Pname": "VANCOUVER4",
		"Pcode": "CAVCJ"
	},
	{
		"Pname": "VANCOUVER4",
		"Pcode": "CAVTM"
	},
	{
		"Pname": "VANCOUVERA",
		"Pcode": "CAVAA"
	},
	{
		"Pname": "VANCOUVERB",
		"Pcode": "CAVAB"
	},
	{
		"Pname": "VANCOUVERC",
		"Pcode": "CAVAC"
	},
	{
		"Pname": "VANCOUVERD",
		"Pcode": "CAVAD"
	},
	{
		"Pname": "VANCOUVERE",
		"Pcode": "CAVAE"
	},

	{
		"Pname": "POINTE NOIRE",
		"Pcode": "CGPNR"
	},
	{
		"Pname": "CHIWAN",
		"Pcode": "CHIWA"
	},
	{
		"Pname": "ABIDJAN",
		"Pcode": "CIABJ"
	},
	{
		"Pname": "ANTOFAGASTA",
		"Pcode": "CLANF"
	},
	{
		"Pname": "ARICA",
		"Pcode": "CLARI"
	},
	{
		"Pname": "IQUIQUE",
		"Pcode": "CLIQQ"
	},
	{
		"Pname": "LIRQUEN",
		"Pcode": "CLLQN"
	},
	{
		"Pname": "MEJILLONES",
		"Pcode": "CLMJS"
	},
	{
		"Pname": "PUARTOANGAMOS",
		"Pcode": "CLPAG"
	},
	{
		"Pname": "SAN ANTONIO",
		"Pcode": "CLSAI"
	},
	{
		"Pname": "SAN ANTOONIO",
		"Pcode": "CLQTN"
	},
	{
		"Pname": "SANVINCENTE",
		"Pcode": "CLSVE"
	},
	{
		"Pname": "TALCAHUANO",
		"Pcode": "CLTHO"
	},
	{
		"Pname": "VALPARAISO",
		"Pcode": "CLVAP"
	},

	{
		"Pname": "DOUALA",
		"Pcode": "CMDLA"
	},
	{
		"Pname": "CHIWAN",
		"Pcode": "CNCWN"
	},
	{
		"Pname": "DACHANBAY",
		"Pcode": "CNDCB"
	},
	{
		"Pname": "DAFENG",
		"Pcode": "CNDFL"
	},
	{
		"Pname": "DAFENG",
		"Pcode": "CNDFE"
	},
	{
		"Pname": "DALIAN",
		"Pcode": "CNDLC"
	},
	{
		"Pname": "FANGCHENG",
		"Pcode": "CNFAN"
	},
	{
		"Pname": "FUQING",
		"Pcode": "CNFUG"
	},
	{
		"Pname": "FUZHOU",
		"Pcode": "CNFOC"
	},
	{
		"Pname": "GZNANSHA",
		"Pcode": "CNGNS"
	},
	{
		"Pname": "HUANG PU",
		"Pcode": "CNHUA"
	},
	{
		"Pname": "HUANGPU",
		"Pcode": "CNHUP"
	},
	{
		"Pname": "JINGTANG",
		"Pcode": "CNJIN"
	},
	{
		"Pname": "JINTANG",
		"Pcode": "CNJIT"
	},
	{
		"Pname": "JINZHOU",
		"Pcode": "CNJNZ"
	},
	{
		"Pname": "LIANYUNGANG",
		"Pcode": "CNLYG"
	},
	{
		"Pname": "LONGKOU",
		"Pcode": "CNLKU"
	},
	{
		"Pname": "NANJING",
		"Pcode": "CNNKG"
	},
	{
		"Pname": "NANJING",
		"Pcode": "CNCNJ"
	},
	{
		"Pname": "NANSHA",
		"Pcode": "CNNNS"
	},
	{
		"Pname": "NANSHA",
		"Pcode": "CNNSA"
	},
	{
		"Pname": "NINGBO",
		"Pcode": "CNNBO"
	},
	{
		"Pname": "NINGBO",
		"Pcode": "CNNGB"
	},
	{
		"Pname": "QINGTAO",
		"Pcode": "CNTAO"
	},
	{
		"Pname": "QINHUANGDAO",
		"Pcode": "CNQHA"
	},
	{
		"Pname": "QUANZHOU",
		"Pcode": "CNQZJ"
	},
	{
		"Pname": "RIZHAO",
		"Pcode": "CNRZH"
	},
	{
		"Pname": "SHANGHAI",
		"Pcode": "CNSH1"
	},
	{
		"Pname": "SHANGHAI",
		"Pcode": "CNSHA"
	},
	{
		"Pname": "SHANGHAI",
		"Pcode": "CNSGH"
	},
	{
		"Pname": "SHANGHAI",
		"Pcode": "CNSH2"
	},
	{
		"Pname": "SHANTOU",
		"Pcode": "CNSWA"
	},
	{
		"Pname": "SHEKOU",
		"Pcode": "CNSKO"
	},
	{
		"Pname": "SHEKOU",
		"Pcode": "CNSHK"
	},
	{
		"Pname": "SHENZEN",
		"Pcode": "CNSZX"
	},
	{
		"Pname": "SHIDAO",
		"Pcode": "CNSHD"
	},
	{
		"Pname": "SKU",
		"Pcode": "CNSKU"
	},
	{
		"Pname": "TAICANG",
		"Pcode": "CNTAG"
	},
	{
		"Pname": "TANGGU",
		"Pcode": "CNTGA"
	},
	{
		"Pname": "TIANJIN",
		"Pcode": "CNTSN"
	},
	{
		"Pname": "VANCOUVER",
		"Pcode": "CNVAA"
	},
	{
		"Pname": "WEIFANG",
		"Pcode": "CNWFG"
	},
	{
		"Pname": "WEIHAI",
		"Pcode": "CNWEI"
	},
	{
		"Pname": "XIAMAN",
		"Pcode": "CNXMN"
	},
	{
		"Pname": "XINGANG",
		"Pcode": "CNXNG"
	},
	{
		"Pname": "XINGANG",
		"Pcode": "CNXGG"
	},
	{
		"Pname": "XINGANG",
		"Pcode": "CNXIN"
	},
	{
		"Pname": "XINGANG",
		"Pcode": "CNTXG"
	},
	{
		"Pname": "XINGANG",
		"Pcode": "CNTXG"
	},
	{
		"Pname": "XINGANG",
		"Pcode": "CNXIG"
	},
	{
		"Pname": "XINGGANG",
		"Pcode": "CNZIN"
	},
	{
		"Pname": "YANGSAN",
		"Pcode": "CNYAN"
	},
	{
		"Pname": "YANGSHAN",
		"Pcode": "CNSHY"
	},
	{
		"Pname": "YANGTAI",
		"Pcode": "CNYAI"
	},
	{
		"Pname": "YANTAICHINA",
		"Pcode": "CNYNT"
	},
	{
		"Pname": "YANTIAN CHINA",
		"Pcode": "CNYTN"
	},
	{
		"Pname": "YINGKOU",
		"Pcode": "CNYIK"
	},
	{
		"Pname": "ZHANG ZHOU",
		"Pcode": "CNZZU"
	},
	{
		"Pname": "ZHANG-JIA-GANG",
		"Pcode": "CNCZG"
	},
	{
		"Pname": "ZJANGJAGANG",
		"Pcode": "CNZJG"
	},
	{
		"Pname": "BARRANQUILLA",
		"Pcode": "COBAQ"
	},
	{
		"Pname": "BUENAVENTURA",
		"Pcode": "COBUN"
	},
	{
		"Pname": "CARTAGENA",
		"Pcode": "COCTG"
	},
	{
		"Pname": "SANTA MARTA",
		"Pcode": "COSMR"
	},
	{
		"Pname": "PORT CALDERA",
		"Pcode": "CRPCD"
	},
	{
		"Pname": "PUERPO CALDERA",
		"Pcode": "CRCAL"
	},
	{
		"Pname": "PUERTO CALDERA",
		"Pcode": "CRPLD"
	},
	{
		"Pname": "PUNTARENAS",
		"Pcode": "CRPAS"
	},
	{
		"Pname": "HABANA",
		"Pcode": "CUHAV"
	},
	{
		"Pname": "FAMAGUSTA",
		"Pcode": "CYFMG"
	},
	{
		"Pname": "LARNACA",
		"Pcode": "CYLCA"
	},
	{
		"Pname": "LIMASSOL",
		"Pcode": "CYLMS"
	},
	{
		"Pname": "WILLEMSTAD",
		"Pcode": "CZWIL"
	},
	{
		"Pname": "ROSTOCK",
		"Pcode": "DDRSK"
	},
	{
		"Pname": "BREMERHAVEN",
		"Pcode": "DEBRV"
	},
	{
		"Pname": "HAMBURG",
		"Pcode": "DEHAM"
	},
	{
		"Pname": "HARBURG",
		"Pcode": "DEHBU"
	},
	{
		"Pname": "HUSUM",
		"Pcode": "DEHUS"
	},
	{
		"Pname": "DJIBOUTI",
		"Pcode": "DJJIB"
	},
	{
		"Pname": "AALBORG",
		"Pcode": "DKAAL"
	},
	{
		"Pname": "AARHUS",
		"Pcode": "DKAAR"
	},
	{
		"Pname": "ESBJERG",
		"Pcode": "DKEBJ"
	},
	{
		"Pname": "KOBENHAVN",
		"Pcode": "DKCPH"
	},
	{
		"Pname": "SANTO DOMINGO",
		"Pcode": "DODOM"
	},
	{
		"Pname": "SANTO DOMINGO",
		"Pcode": "DOSDQ"
	},
	{
		"Pname": "ANNABA",
		"Pcode": "DZAAE"
	},
	{
		"Pname": "BEJAIA",
		"Pcode": "DZBJA"
	},
	{
		"Pname": "ORAN",
		"Pcode": "DZORN"
	},
	{
		"Pname": "ESMERALDAS",
		"Pcode": "ECESM"
	},
	{
		"Pname": "GUAYAQUIL",
		"Pcode": "ECGYE"
	},
	{
		"Pname": "ADABIYA",
		"Pcode": "EGADA"
	},
	{
		"Pname": "ALEXANDRIA",
		"Pcode": "EGALY"
	},
	{
		"Pname": "DAMIETTA",
		"Pcode": "EGDAM"
	},
	{
		"Pname": "ELDEKHEILA",
		"Pcode": "EGEDK"
	},
	{
		"Pname": "MANTA",
		"Pcode": "EGMEC"
	},
	{
		"Pname": "PORT SAID",
		"Pcode": "EGPSD"
	},
	{
		"Pname": "SOKHNA",
		"Pcode": "EGSOK"
	},
	{
		"Pname": "SOKHNA",
		"Pcode": "EGAIS"
	},
	{
		"Pname": "SUEZ",
		"Pcode": "EGSUZ"
	},
	{
		"Pname": "UCSAM",
		"Pcode": "EGUCS"
	},

	{
		"Pname": "ALGECIRAS",
		"Pcode": "ESALG"
	},
	{
		"Pname": "ALGMED",
		"Pcode": "ESAMD"
	},
	{
		"Pname": "BARCELONA",
		"Pcode": "ESBCN"
	},
	{
		"Pname": "BILBAO",
		"Pcode": "ESBIO"
	},
	{
		"Pname": "CEUTA",
		"Pcode": "ESCEU"
	},
	{
		"Pname": "LAS PALMAS",
		"Pcode": "ESLPA"
	},
	{
		"Pname": "MALAGA",
		"Pcode": "ESAGP"
	},
	{
		"Pname": "MELILLA",
		"Pcode": "ESMLN"
	},
	{
		"Pname": "PALMA DE MALLORCA",
		"Pcode": "ESPMI"
	},
	{
		"Pname": "VALENCIA",
		"Pcode": "ESVLC"
	},

	{
		"Pname": "ASSAB",
		"Pcode": "ETASA"
	},
	{
		"Pname": "HELSINKI",
		"Pcode": "FIHEL"
	},
	{
		"Pname": "RAUMA",
		"Pcode": "FIRAU"
	},
	{
		"Pname": "TURKU",
		"Pcode": "FITKU"
	},
	{
		"Pname": "LAUTOKA",
		"Pcode": "FJLTK"
	},
	{
		"Pname": "SU5",
		"Pcode": "FJSU5"
	},
	{
		"Pname": "SUVA",
		"Pcode": "FJSUV"
	},
	{
		"Pname": "BORDEAUX",
		"Pcode": "FRBOD"
	},
	{
		"Pname": "CHERBOURG",
		"Pcode": "FRCER"
	},
	{
		"Pname": "FOS",
		"Pcode": "FRFOS"
	},
	{
		"Pname": "LAVERA",
		"Pcode": "FRLAV"
	},
	{
		"Pname": "LE HAVRE",
		"Pcode": "FRLEH"
	},
	{
		"Pname": "MARSEILLE",
		"Pcode": "FRMRS"
	},

	{
		"Pname": "LIBREVILLE",
		"Pcode": "GALBV"
	},
	{
		"Pname": "ABERDEEN",
		"Pcode": "GBABD"
	},
	{
		"Pname": "AVONMOUTH",
		"Pcode": "GBAVO"
	},
	{
		"Pname": "EDINBURGH",
		"Pcode": "GBEDI"
	},
	{
		"Pname": "FELIXSTOWE",
		"Pcode": "GBFXT"
	},
	{
		"Pname": "GARSTON",
		"Pcode": "GBGTN"
	},
	{
		"Pname": "GLASGOW",
		"Pcode": "GBGLW"
	},
	{
		"Pname": "GRANGEMOUTH",
		"Pcode": "GBGRG"
	},
	{
		"Pname": "HARTLEPOOL",
		"Pcode": "GBHTP"
	},
	{
		"Pname": "HARWICH",
		"Pcode": "GBHRW"
	},
	{
		"Pname": "HOLYHEAD",
		"Pcode": "GBHHD"
	},
	{
		"Pname": "HULL",
		"Pcode": "GBHUL"
	},
	{
		"Pname": "IPSWICH",
		"Pcode": "GBIPS"
	},
	{
		"Pname": "LEITH",
		"Pcode": "GBLEI"
	},
	{
		"Pname": "LIVERPOOL",
		"Pcode": "GBLIV"
	},
	{
		"Pname": "LONDON",
		"Pcode": "GBLON"
	},
	{
		"Pname": "MANCHESTER",
		"Pcode": "GBMNC"
	},
	{
		"Pname": "NEWCASTLE UPON TYNE",
		"Pcode": "GBNCL"
	},
	{
		"Pname": "PORTSMOUTH",
		"Pcode": "GBPME"
	},
	{
		"Pname": "SOUTHAMPTON",
		"Pcode": "GBSOU"
	},
	{
		"Pname": "TEMS PORT",
		"Pcode": "GBTHP"
	},
	{
		"Pname": "TILBURY",
		"Pcode": "GBTIL"
	},
	{
		"Pname": "TYNE",
		"Pcode": "GBTYN"
	},

	{
		"Pname": "TAKORADI",
		"Pcode": "GHTKD"
	},
	{
		"Pname": "TEMA",
		"Pcode": "GHTEM"
	},
	{
		"Pname": "GIBRALTAR",
		"Pcode": "GIGIB"
	},
	{
		"Pname": "BANJUL",
		"Pcode": "GMBJL"
	},
	{
		"Pname": "CONAKRY",
		"Pcode": "GNCKY"
	},
	{
		"Pname": "POINTE A PITRE",
		"Pcode": "GPPTP"
	},
	{
		"Pname": "AST",
		"Pcode": "GRAST"
	},
	{
		"Pname": "ATHENS",
		"Pcode": "GRATH"
	},
	{
		"Pname": "HER",
		"Pcode": "GRHER"
	},
	{
		"Pname": "PIRAEUS",
		"Pcode": "GRPIR"
	},
	{
		"Pname": "THESSALONIKI",
		"Pcode": "GRSKG"
	},

	{
		"Pname": "PUEROTO-QUETZAL",
		"Pcode": "GTPTQ"
	},
	{
		"Pname": "PUERTO QUETZAL",
		"Pcode": "GTPRQ"
	},
	{
		"Pname": "SAN JOSE",
		"Pcode": "GTSNJ"
	},
	{
		"Pname": "GUAM",
		"Pcode": "GUGUM"
	},
	{
		"Pname": "GEORGETOWN",
		"Pcode": "GYGEO"
	},
	{
		"Pname": "HONGKONG",
		"Pcode": "HKHKM"
	},
	{
		"Pname": "HONGKONG",
		"Pcode": "HKHIT"
	},
	{
		"Pname": "HONGKONG",
		"Pcode": "HKHKG"
	},
	{
		"Pname": "HONGKONG",
		"Pcode": "HKKWN"
	},
	{
		"Pname": "AMAPALA",
		"Pcode": "HNAMP"
	},
	{
		"Pname": "PUERTO CORTES",
		"Pcode": "HNPCR"
	},
	{
		"Pname": "SAN LORENZO",
		"Pcode": "HNSLO"
	},
	{
		"Pname": "RIJEKA",
		"Pcode": "HRRJK"
	},
	{
		"Pname": "PORT AU PRINCE",
		"Pcode": "HTPAP"
	},
	{
		"Pname": "BALIKPAPAN",
		"Pcode": "IDBPN"
	},
	{
		"Pname": "BELAWAN",
		"Pcode": "IDBLW"
	},
	{
		"Pname": "DUMAI",
		"Pcode": "IDDUM"
	},
	{
		"Pname": "JAKARTTA",
		"Pcode": "IDJKT"
	},
	{
		"Pname": "MEDAN",
		"Pcode": "IDMES"
	},
	{
		"Pname": "PADANG",
		"Pcode": "IDPDG"
	},
	{
		"Pname": "PALEMBANG",
		"Pcode": "IDPLM"
	},
	{
		"Pname": "PONTIANAK",
		"Pcode": "IDPNK"
	},
	{
		"Pname": "SEMARANG",
		"Pcode": "IDSRG"
	},
	{
		"Pname": "SURABAYA",
		"Pcode": "IDSUB"
	},
	{
		"Pname": "UJUNG PANDANG",
		"Pcode": "IDUPG"
	},
	{
		"Pname": "CORK",
		"Pcode": "IEORK"
	},
	{
		"Pname": "DUBLIN",
		"Pcode": "IEDUB"
	},
	{
		"Pname": "ASHDOD",
		"Pcode": "ILASH"
	},
	{
		"Pname": "ELAT",
		"Pcode": "ILETH"
	},
	{
		"Pname": "HAIFA",
		"Pcode": "ILHFA"
	},
	{
		"Pname": "BOMBAY",
		"Pcode": "INBOM"
	},
	{
		"Pname": "CALCUTTA",
		"Pcode": "INCCU"
	},
	{
		"Pname": "COCHIN",
		"Pcode": "INCOK"
	},
	{
		"Pname": "JAWAHARLAL NEHRU",
		"Pcode": "INJHT"
	},
	{
		"Pname": "KANDLA",
		"Pcode": "INIXY"
	},
	{
		"Pname": "MADRAS",
		"Pcode": "INMAA"
	},
	{
		"Pname": "MAHE",
		"Pcode": "INMAH"
	},
	{
		"Pname": "MANGALORE",
		"Pcode": "INIXE"
	},
	{
		"Pname": "MUN",
		"Pcode": "INMUN"
	},
	{
		"Pname": "MUNDRA",
		"Pcode": "INMDA"
	},
	{
		"Pname": "NANSHA",
		"Pcode": "INNSH"
	},
	{
		"Pname": "NHAVASHEVA",
		"Pcode": "INNSA"
	},
	{
		"Pname": "VISAKHAPATNAM",
		"Pcode": "INVTZ"
	},
	{
		"Pname": "BAGHDAD",
		"Pcode": "IQBGW"
	},
	{
		"Pname": "BANDAR KHOMEINI",
		"Pcode": "IRBKM"
	},
	{
		"Pname": "BANDARABBAS",
		"Pcode": "IRBSR"
	},
	{
		"Pname": "BANDARABBAS",
		"Pcode": "IRBND"
	},
	{
		"Pname": "BANDARABBAS",
		"Pcode": "IRSRP"
	},
	{
		"Pname": "KHORRAMSHAHR",
		"Pcode": "IRKHO"
	},
	{
		"Pname": "REYKJAVIK",
		"Pcode": "ISREK"
	},
	{
		"Pname": "CAGLIARI",
		"Pcode": "ITCAG"
	},
	{
		"Pname": "CATANIA",
		"Pcode": "ITCTA"
	},
	{
		"Pname": "GENOA",
		"Pcode": "ITGOA"
	},
	{
		"Pname": "GIOIA TAURO",
		"Pcode": "ITGIT"
	},
	{
		"Pname": "GIOIATARO",
		"Pcode": "ITGTO"
	},
	{
		"Pname": "LA SPEZIA",
		"Pcode": "ITSPE"
	},
	{
		"Pname": "NAPOLI",
		"Pcode": "ITNPO"
	},
	{
		"Pname": "NAPOLI",
		"Pcode": "ITNAP"
	},
	{
		"Pname": "OTHER-PORT",
		"Pcode": "ITZZZ"
	},
	{
		"Pname": "PALERMO",
		"Pcode": "ITPMO"
	},
	{
		"Pname": "RAVENNA",
		"Pcode": "ITRAN"
	},
	{
		"Pname": "SALERNO",
		"Pcode": "ITSAL"
	},
	{
		"Pname": "SAVONA",
		"Pcode": "ITSVN"
	},
	{
		"Pname": "TARANTO",
		"Pcode": "ITTAR"
	},
	{
		"Pname": "TERMINIIMERESS",
		"Pcode": "ITTRI"
	},
	{
		"Pname": "TRIESTE",
		"Pcode": "ITTRS"
	},
	{
		"Pname": "VENEZIA",
		"Pcode": "ITVCE"
	},
	{
		"Pname": "VPI",
		"Pcode": "ITVPI"
	},
	{
		"Pname": "KINGSTON",
		"Pcode": "JMKIN"
	},
	{
		"Pname": "AQABA",
		"Pcode": "JOAQJ"
	},

	{
		"Pname": "ABURACHU",
		"Pcode": "JPABR"
	},
	{
		"Pname": "ABURATSU",
		"Pcode": "JPABU"
	},
	{
		"Pname": "AKITA",
		"Pcode": "JPAXT"
	},
	{
		"Pname": "AOI",
		"Pcode": "JPAIO"
	},
	{
		"Pname": "CHIBA",
		"Pcode": "JPCHB"
	},
	{
		"Pname": "DEJIMA",
		"Pcode": "JPDEJ"
	},
	{
		"Pname": "FUKUYAMA",
		"Pcode": "JPFKY"
	},
	{
		"Pname": "FUNABASHI",
		"Pcode": "JPFNB"
	},
	{
		"Pname": "FUSHIKI",
		"Pcode": "JPFSK"
	},
	{
		"Pname": "HACHINOHE",
		"Pcode": "JPHHE"
	},
	{
		"Pname": "HAKATA",
		"Pcode": "JPHKT"
	},
	{
		"Pname": "HAKODATE",
		"Pcode": "JPHKP"
	},
	{
		"Pname": "HAKODATE",
		"Pcode": "JPHKD"
	},
	{
		"Pname": "HAMADA",
		"Pcode": "JPHMD"
	},
	{
		"Pname": "HIBIKI",
		"Pcode": "JPHBK"
	},
	{
		"Pname": "HIBIKI",
		"Pcode": "JPTBT"
	},
	{
		"Pname": "HIROHATA",
		"Pcode": "JPHRH"
	},
	{
		"Pname": "HIROSHIMA",
		"Pcode": "JPHIJ"
	},
	{
		"Pname": "HIROSHIMA",
		"Pcode": "JPHIR"
	},
	{
		"Pname": "HITACHINAKA",
		"Pcode": "JPHIC"
	},
	{
		"Pname": "HOSOSHIMA",
		"Pcode": "JPHSM"
	},
	{
		"Pname": "IMABARI",
		"Pcode": "JPIMB"
	},
	{
		"Pname": "IMARI",
		"Pcode": "JPIMI"
	},
	{
		"Pname": "ISHIKARI",
		"Pcode": "JPISI"
	},
	{
		"Pname": "ISHIKARIWAN-SHINKO",
		"Pcode": "JPISS"
	},
	{
		"Pname": "IWAKUNI",
		"Pcode": "JPIWK"
	},
	{
		"Pname": "IYOMISHIMA",
		"Pcode": "JPIYM"
	},
	{
		"Pname": "IYOMISHIMA",
		"Pcode": "JPMKX"
	},
	{
		"Pname": "KAGOSHIMA",
		"Pcode": "JPKOJ"
	},
	{
		"Pname": "KAGOSHIMA SENDAI",
		"Pcode": "JPSEN"
	},
	{
		"Pname": "KANAZAWA",
		"Pcode": "JPKNZ"
	},
	{
		"Pname": "KASHIMA",
		"Pcode": "JPKSM"
	},
	{
		"Pname": "KAWASAKI",
		"Pcode": "JPKWS"
	},
	{
		"Pname": "KINUURA",
		"Pcode": "JPKNU"
	},
	{
		"Pname": "KOBE",
		"Pcode": "JPUKB"
	},
	{
		"Pname": "KOBE",
		"Pcode": "JPKOB"
	},
	{
		"Pname": "KOBE",
		"Pcode": "JPUBK"
	},
	{
		"Pname": "KOKURA",
		"Pcode": "JPKOK"
	},
	{
		"Pname": "KOTI",
		"Pcode": "JPKCZ"
	},
	{
		"Pname": "KURE",
		"Pcode": "JPKRE"
	},
	{
		"Pname": "KUSHIRO",
		"Pcode": "JPKUH"
	},
	{
		"Pname": "MAIZURU",
		"Pcode": "JPMAI"
	},
	{
		"Pname": "MATSUYAMA",
		"Pcode": "JPMYJ"
	},
	{
		"Pname": "MIIKE",
		"Pcode": "JPMII"
	},
	{
		"Pname": "MISHIMA-KAWANOE",
		"Pcode": "JPMKK"
	},
	{
		"Pname": "MISUMI",
		"Pcode": "JPMIS"
	},
	{
		"Pname": "MIYAGI SENDAI",
		"Pcode": "JPSDJ"
	},
	{
		"Pname": "MIZUSHIMA",
		"Pcode": "JPMIJ"
	},
	{
		"Pname": "MIZUSHIMA",
		"Pcode": "JPMIZ"
	},
	{
		"Pname": "MOJI",
		"Pcode": "JPMOJ"
	},
	{
		"Pname": "MURORAN",
		"Pcode": "JPMUR"
	},
	{
		"Pname": "NAGASAKI",
		"Pcode": "JPNGS"
	},
	{
		"Pname": "NAGOYA",
		"Pcode": "JPNGO"
	},
	{
		"Pname": "NAKANOSEKI",
		"Pcode": "JPNKN"
	},
	{
		"Pname": "NAKANOSEKI",
		"Pcode": "JPNAN"
	},
	{
		"Pname": "NAOETSU",
		"Pcode": "JPNAO"
	},
	{
		"Pname": "NIGATA",
		"Pcode": "JPKIJ"
	},
	{
		"Pname": "NIGATA",
		"Pcode": "JPNIH"
	},
	{
		"Pname": "OITA",
		"Pcode": "JPOIP"
	},
	{
		"Pname": "OITA",
		"Pcode": "JPOIT"
	},
	{
		"Pname": "OKINAWA",
		"Pcode": "JPNAH"
	},
	{
		"Pname": "ONAHAMA",
		"Pcode": "JPONA"
	},
	{
		"Pname": "OSAKA",
		"Pcode": "JPOSA"
	},
	{
		"Pname": "OTAKE",
		"Pcode": "JPOTK"
	},
	{
		"Pname": "SAEKI",
		"Pcode": "JPSAE"
	},
	{
		"Pname": "SAKA",
		"Pcode": "JPSMN"
	},
	{
		"Pname": "SAKAIDE",
		"Pcode": "JPSKD"
	},
	{
		"Pname": "SAKATA",
		"Pcode": "JPSKT"
	},
	{
		"Pname": "SASEBO",
		"Pcode": "JPSSB"
	},
	{
		"Pname": "SENDAISHINKO",
		"Pcode": "JPSDS"
	},
	{
		"Pname": "SENDAISHIOGAMAKO",
		"Pcode": "JPSGM"
	},
	{
		"Pname": "SHIBUSHI",
		"Pcode": "JPSBS"
	},
	{
		"Pname": "SHIMIZU",
		"Pcode": "JPSMZ"
	},
	{
		"Pname": "SHIMONOSEKI",
		"Pcode": "JPSHS"
	},
	{
		"Pname": "TAKAMATSU",
		"Pcode": "JPTAK"
	},
	{
		"Pname": "TAKUMA",
		"Pcode": "JPTKM"
	},
	{
		"Pname": "TOKUSHIMA",
		"Pcode": "JPTKS"
	},
	{
		"Pname": "TOKUYAMA",
		"Pcode": "JPTKY"
	},
	{
		"Pname": "TOKYO",
		"Pcode": "JPTYO"
	},
	{
		"Pname": "TOKYO",
		"Pcode": "JPTOK"
	},
	{
		"Pname": "TOMAKOMAI",
		"Pcode": "JPTMK"
	},
	{
		"Pname": "TOYAMASHINKO",
		"Pcode": "JPTOS"
	},
	{
		"Pname": "TOYAMASHINKO",
		"Pcode": "JPTOY"
	},
	{
		"Pname": "TOYAMASHINKO",
		"Pcode": "JPTSH"
	},
	{
		"Pname": "TOYOHASHI",
		"Pcode": "JPTHS"
	},
	{
		"Pname": "TSURUGA",
		"Pcode": "JPTRG"
	},
	{
		"Pname": "UBE",
		"Pcode": "JPUBJ"
	},
	{
		"Pname": "UNO",
		"Pcode": "JPUNO"
	},
	{
		"Pname": "WAKAYAMA",
		"Pcode": "JPWAK"
	},
	{
		"Pname": "YATSUSHIRO",
		"Pcode": "JPYAT"
	},
	{
		"Pname": "YAWATAHAMA",
		"Pcode": "JPYWH"
	},
	{
		"Pname": "YOKKAICHI",
		"Pcode": "JPYKK"
	},
	{
		"Pname": "YOKOHAMA",
		"Pcode": "JPYOK"
	},

	{
		"Pname": "MOMBASA",
		"Pcode": "KEMBA"
	},
	{
		"Pname": "KOSTM",
		"Pcode": "KHKOS"
	},
	{
		"Pname": "SIHANOUKVILLE",
		"Pcode": "KHSHV"
	},
	{
		"Pname": "SIHANOUKVILLE",
		"Pcode": "KHSIH"
	},
	{
		"Pname": "TARAWA",
		"Pcode": "KITRW"
	},
	{
		"Pname": "MORONI",
		"Pcode": "KMYVA"
	},
	{
		"Pname": "CHEONGIN",
		"Pcode": "KPJC*"
	},
	{
		"Pname": "CHINNAMPO",
		"Pcode": "KPHC*"
	},
	{
		"Pname": "CHONG JIN",
		"Pcode": "KPCHO"
	},
	{
		"Pname": "HAEJU",
		"Pcode": "KPJH*"
	},
	{
		"Pname": "HUNGNAM",
		"Pcode": "KPHGM"
	},
	{
		"Pname": "NAJIN",
		"Pcode": "KPJN*"
	},
	{
		"Pname": "NAMPO",
		"Pcode": "KPNAM"
	},
	{
		"Pname": "SINPO",
		"Pcode": "KPNS*"
	},
	{
		"Pname": "SINUIJU",
		"Pcode": "KPJS*"
	},
	{
		"Pname": "WEONSAN",
		"Pcode": "KPSW*"
	},
	{
		"Pname": "ALL PORTS",
		"Pcode": "KRALL"
	},
	{
		"Pname": "CHEJU",
		"Pcode": "KRCHA"
	},
	{
		"Pname": "CHUNGMU",
		"Pcode": "KRCM*"
	},
	{
		"Pname": "DAESAN",
		"Pcode": "KRDAS"
	},
	{
		"Pname": "DONGHAE",
		"Pcode": "KRDH*"
	},
	{
		"Pname": "GAMMAN PORT",
		"Pcode": "KRGAM"
	},
	{
		"Pname": "INCHEUN",
		"Pcode": "KRINC"
	},
	{
		"Pname": "JANGSEUNGPO",
		"Pcode": "KRJP*"
	},
	{
		"Pname": "JINHAE",
		"Pcode": "KRCHF"
	},
	{
		"Pname": "KOHYUN",
		"Pcode": "KRKH*"
	},
	{
		"Pname": "KOJUNG",
		"Pcode": "KRKJ*"
	},
	{
		"Pname": "KUNSAN",
		"Pcode": "KRKUV"
	},
	{
		"Pname": "KWANG YANG",
		"Pcode": "KRKAN"
	},
	{
		"Pname": "KWANG YANG",
		"Pcode": "KRKWY"
	},
	{
		"Pname": "MASAN",
		"Pcode": "KRMAS"
	},
	{
		"Pname": "MIPO",
		"Pcode": "KRMI*"
	},
	{
		"Pname": "MOKPO",
		"Pcode": "KRMOK"
	},
	{
		"Pname": "MUKHO",
		"Pcode": "KRMUK"
	},
	{
		"Pname": "OKPO",
		"Pcode": "KROP*"
	},
	{
		"Pname": "ONSAN",
		"Pcode": "KROS*"
	},

	{
		"Pname": "PECT PORT",
		"Pcode": "KRPET"
	},
	{
		"Pname": "POHANG",
		"Pcode": "KRKPO"
	},
	{
		"Pname": "PUNSANNEWPORT",
		"Pcode": "KRPNC"
	},
	{
		"Pname": "PUSAN",
		"Pcode": "KRKJC"
	},
	{
		"Pname": "PUSAN",
		"Pcode": "KRPUS"
	},
	{
		"Pname": "PUSANNEWPORT",
		"Pcode": "KRPU1"
	},
	{
		"Pname": "PUSANNEWPORT",
		"Pcode": "KRPUN"
	},
	{
		"Pname": "PYEONGTAEK",
		"Pcode": "KRPYT"
	},
	{
		"Pname": "PYONGTACK",
		"Pcode": "KRPTK"
	},
	{
		"Pname": "SAMCHEONPO",
		"Pcode": "KRSP*"
	},
	{
		"Pname": "SAMCHOK",
		"Pcode": "KRSUK"
	},
	{
		"Pname": "SEOGWIPO",
		"Pcode": "KRSG*"
	},
	{
		"Pname": "SINHANG",
		"Pcode": "KRSH*"
	},
	{
		"Pname": "SOCKCHO",
		"Pcode": "KRSHO"
	},
	{
		"Pname": "ULSAN",
		"Pcode": "KRUSN"
	},
	{
		"Pname": "WANDO",
		"Pcode": "KRWD*"
	},
	{
		"Pname": "YEOCHEON",
		"Pcode": "KRYC*"
	},
	{
		"Pname": "YOSU",
		"Pcode": "KRYOS"
	},

	{
		"Pname": "KUWAIT",
		"Pcode": "KWKWI"
	},
	{
		"Pname": "BEIRUT",
		"Pcode": "LBBEY"
	},
	{
		"Pname": "COLOMBO",
		"Pcode": "LKCMB"
	},
	{
		"Pname": "TRINCOMALEE",
		"Pcode": "LKTRR"
	},
	{
		"Pname": "MONROVIA",
		"Pcode": "LRMLW"
	},
	{
		"Pname": "MISRATAH",
		"Pcode": "LYMRA"
	},
	{
		"Pname": "TRIPOLI",
		"Pcode": "LYTIP"
	},
	{
		"Pname": "CASABLANCA",
		"Pcode": "MACAS"
	},
	{
		"Pname": "MAJUNGA",
		"Pcode": "MGMJN"
	},
	{
		"Pname": "TAMATAVE",
		"Pcode": "MGTMM"
	},
	{
		"Pname": "MAJURO",
		"Pcode": "MHMAJ"
	},
	{
		"Pname": "RANGOON",
		"Pcode": "MMRGN"
	},
	{
		"Pname": "MACAU",
		"Pcode": "MOMAC"
	},
	{
		"Pname": "NOUAKCHOTT",
		"Pcode": "MRNKC"
	},
	{
		"Pname": "MALTA",
		"Pcode": "MTMLA"
	},
	{
		"Pname": "MALTA",
		"Pcode": "MTMAR"
	},
	{
		"Pname": "PORT LOUIS",
		"Pcode": "MUPLU"
	},
	{
		"Pname": "ACAPULCO",
		"Pcode": "MXACA"
	},
	{
		"Pname": "ENSENADA",
		"Pcode": "MXESE"
	},
	{
		"Pname": "GUAYMAS",
		"Pcode": "MXGYM"
	},
	{
		"Pname": "LAZALLINO",
		"Pcode": "MXZL2"
	},
	{
		"Pname": "LAZARO",
		"Pcode": "MXLCS"
	},
	{
		"Pname": "LAZARO CARDENAS",
		"Pcode": "MXLZC"
	},
	{
		"Pname": "LAZAROCAROENAS",
		"Pcode": "MXLZ2"
	},
	{
		"Pname": "LAZAROCAROENAS",
		"Pcode": "MXLZ1"
	},
	{
		"Pname": "LAZAROCAROENAS",
		"Pcode": "MXZLC"
	},
	{
		"Pname": "MANZANILLO",
		"Pcode": "MXZLO"
	},
	{
		"Pname": "MAZATLAN",
		"Pcode": "MXMZT"
	},
	{
		"Pname": "PANTACO",
		"Pcode": "MXPAN"
	},
	{
		"Pname": "SALINA CRUZ",
		"Pcode": "MXSAC"
	},
	{
		"Pname": "TAMPICO",
		"Pcode": "MXTAM"
	},
	{
		"Pname": "VERACRUZ",
		"Pcode": "MXVER"
	},
	{
		"Pname": "BINTULU",
		"Pcode": "MYBTU"
	},
	{
		"Pname": "JAMBONGAN",
		"Pcode": "MYJAM"
	},
	{
		"Pname": "JOHOR",
		"Pcode": "MYJOH"
	},
	{
		"Pname": "KOTA KINABALU",
		"Pcode": "MYBKI"
	},
	{
		"Pname": "KUCHING",
		"Pcode": "MYKCH"
	},
	{
		"Pname": "KUDAT",
		"Pcode": "MYKUD"
	},
	{
		"Pname": "LABUAN",
		"Pcode": "MYLBU"
	},
	{
		"Pname": "LAHAD DATU",
		"Pcode": "MYLDU"
	},
	{
		"Pname": "LAWAS",
		"Pcode": "MYLWY"
	},
	{
		"Pname": "LIMBANG",
		"Pcode": "MYLMN"
	},
	{
		"Pname": "MIRI",
		"Pcode": "MYMYY"
	},
	{
		"Pname": "PASIR GUDANG",
		"Pcode": "MYPGU"
	},
	{
		"Pname": "PENANG",
		"Pcode": "MYPEN"
	},
	{
		"Pname": "PORTKELANG",
		"Pcode": "MYPKL"
	},
	{
		"Pname": "PORTKELANG",
		"Pcode": "MYPKG"
	},
	{
		"Pname": "SANDAKAN",
		"Pcode": "MYSDK"
	},
	{
		"Pname": "SIBU",
		"Pcode": "MYSBW"
	},
	{
		"Pname": "TANJUNG PELEPAS",
		"Pcode": "MYTPP"
	},
	{
		"Pname": "TAWAU",
		"Pcode": "MYTWU"
	},
	{
		"Pname": "WESTON",
		"Pcode": "MYWES"
	},
	{
		"Pname": "BEIRA",
		"Pcode": "MZBEW"
	},
	{
		"Pname": "MAPUTO",
		"Pcode": "MZMPM"
	},
	{
		"Pname": "WALVIS BAY",
		"Pcode": "NAWVB"
	},
	{
		"Pname": "NOUMEA",
		"Pcode": "NCNOU"
	},

	{
		"Pname": "APAPA",
		"Pcode": "NGAPP"
	},
	{
		"Pname": "CALABAR",
		"Pcode": "NGCBG"
	},
	{
		"Pname": "LAGOS",
		"Pcode": "NGLOS"
	},
	{
		"Pname": "PORT HARCOURT",
		"Pcode": "NGPHC"
	},
	{
		"Pname": "TINCANISLAND",
		"Pcode": "NGTIN"
	},
	{
		"Pname": "WARRI",
		"Pcode": "NGWAR"
	},
	{
		"Pname": "CORINTO",
		"Pcode": "NICIO"
	},
	{
		"Pname": "AMSTERDAM",
		"Pcode": "NLAMS"
	},
	{
		"Pname": "ROTTERDAM",
		"Pcode": "NLRTM"
	},
	{
		"Pname": "ALESUND",
		"Pcode": "NOAES"
	},
	{
		"Pname": "DRAMMEN",
		"Pcode": "NODRM"
	},
	{
		"Pname": "OSLO",
		"Pcode": "NOOSL"
	},
	{
		"Pname": "NAURU ISLAND",
		"Pcode": "NRINU"
	},

	{
		"Pname": "AUCKLAND",
		"Pcode": "NZAKL"
	},
	{
		"Pname": "BLUFF HARBOUR",
		"Pcode": "NZBLU"
	},
	{
		"Pname": "CHRISTCHURCH",
		"Pcode": "NZCHC"
	},
	{
		"Pname": "DUNEDIN",
		"Pcode": "NZDUD"
	},
	{
		"Pname": "LYTTELTON",
		"Pcode": "NZLYT"
	},
	{
		"Pname": "NAPIER",
		"Pcode": "NZNPE"
	},
	{
		"Pname": "NELSON",
		"Pcode": "NZNSN"
	},
	{
		"Pname": "NEW PLYMOUTH",
		"Pcode": "NZNPL"
	},
	{
		"Pname": "OTAGO HARBOUR",
		"Pcode": "NZORR"
	},
	{
		"Pname": "PORT CHALMERS",
		"Pcode": "NZPOE"
	},
	{
		"Pname": "TAURANGA",
		"Pcode": "NZTRG"
	},
	{
		"Pname": "TIMARU",
		"Pcode": "NZTIU"
	},
	{
		"Pname": "WELLINGTON",
		"Pcode": "NZWLG"
	},
	{
		"Pname": "MUSCAT",
		"Pcode": "OMMCT"
	},
	{
		"Pname": "SALALAH",
		"Pcode": "OMSLL"
	},
	{
		"Pname": "SALALAH",
		"Pcode": "OMSLV"
	},
	{
		"Pname": "SOHAR",
		"Pcode": "OMSOH"
	},

	{
		"Pname": "BALBOA",
		"Pcode": "PABLB"
	},
	{
		"Pname": "COLON",
		"Pcode": "PAONX"
	},
	{
		"Pname": "CRISTOBAL",
		"Pcode": "PACTB"
	},
	{
		"Pname": "MANZANILLO",
		"Pcode": "PAMIT"
	},
	{
		"Pname": "MANZANILLO",
		"Pcode": "PAMAN"
	},
	{
		"Pname": "PAMZX",
		"Pcode": "PAMZX"
	},
	{
		"Pname": "PANAMA CITY",
		"Pcode": "PAPTY"
	},
	{
		"Pname": "PONAPE",
		"Pcode": "PCPNI"
	},
	{
		"Pname": "SAIPAN",
		"Pcode": "PCSPN"
	},
	{
		"Pname": "TRUK",
		"Pcode": "PCTKK"
	},
	{
		"Pname": "YAP",
		"Pcode": "PCYAP"
	},
	{
		"Pname": "CALLAO",
		"Pcode": "PECLF"
	},
	{
		"Pname": "CALLAO",
		"Pcode": "PECLL"
	},
	{
		"Pname": "CHIMBOTE",
		"Pcode": "PECHM"
	},
	{
		"Pname": "ILLO",
		"Pcode": "PEPIL"
	},
	{
		"Pname": "ILO",
		"Pcode": "PEILO"
	},
	{
		"Pname": "MATARANI",
		"Pcode": "PEMRI"
	},
	{
		"Pname": "PAITA",
		"Pcode": "PEPAI"
	},
	{
		"Pname": "SANJUAN",
		"Pcode": "PESJA"
	},

	{
		"Pname": "PAPEETE",
		"Pcode": "PFPPT"
	},
	{
		"Pname": "PP5",
		"Pcode": "PFPP5"
	},
	{
		"Pname": "ALOTAU",
		"Pcode": "PGGUR"
	},
	{
		"Pname": "KAVIENG",
		"Pcode": "PGKVG"
	},
	{
		"Pname": "KIMBE",
		"Pcode": "PGKIM"
	},
	{
		"Pname": "LAE",
		"Pcode": "PGLAE"
	},
	{
		"Pname": "MADANG",
		"Pcode": "PGMAG"
	},
	{
		"Pname": "OROBAY",
		"Pcode": "PGROR"
	},
	{
		"Pname": "PORT MORESBY",
		"Pcode": "PGPOM"
	},
	{
		"Pname": "RABAUL",
		"Pcode": "PGRAB"
	},
	{
		"Pname": "WEWAK",
		"Pcode": "PGWWK"
	},
	{
		"Pname": "BATANGAS",
		"Pcode": "PHBTG"
	},
	{
		"Pname": "BUGO",
		"Pcode": "PHBUG"
	},
	{
		"Pname": "CAGAYAN DE ORO",
		"Pcode": "PHCGY"
	},
	{
		"Pname": "CEBU",
		"Pcode": "PHCEB"
	},
	{
		"Pname": "DAVAO",
		"Pcode": "PHDVO"
	},
	{
		"Pname": "GENERAL SANTOS",
		"Pcode": "PHGES"
	},
	{
		"Pname": "LUGAIT",
		"Pcode": "PHLUG"
	},
	{
		"Pname": "MANILA",
		"Pcode": "PHMNN"
	},
	{
		"Pname": "MANILA",
		"Pcode": "PHMNL"
	},
	{
		"Pname": "MANILA",
		"Pcode": "PHMLL"
	},
	{
		"Pname": "MANILA SOUTH PORT",
		"Pcode": "PHMJJ"
	},
	{
		"Pname": "PORO",
		"Pcode": "PHPRO"
	},
	{
		"Pname": "PUERTO PRINCESA",
		"Pcode": "PHPPS"
	},
	{
		"Pname": "SAN FERNANDO",
		"Pcode": "PHSFE"
	},
	{
		"Pname": "SANGI",
		"Pcode": "PHSNG"
	},
	{
		"Pname": "SOUTH MANILA",
		"Pcode": "PHMIP"
	},
	{
		"Pname": "SOUTH MANILA",
		"Pcode": "PHMNS"
	},
	{
		"Pname": "SUBIC",
		"Pcode": "PHSBC"
	},
	{
		"Pname": "TIGARD",
		"Pcode": "PHTGO"
	},

	{
		"Pname": "KARACHI",
		"Pcode": "PKKHI"
	},
	{
		"Pname": "PORTQASIM",
		"Pcode": "PKBQM"
	},
	{
		"Pname": "PORTQASIM",
		"Pcode": "PKPQ1"
	},
	{
		"Pname": "GDANSK",
		"Pcode": "PLGDN"
	},
	{
		"Pname": "GDYNIA",
		"Pcode": "PLGDY"
	},
	{
		"Pname": "SANJUAN",
		"Pcode": "PRSJU"
	},
	{
		"Pname": "LISBON",
		"Pcode": "PTLIS"
	},
	{
		"Pname": "SINES",
		"Pcode": "PTSIE"
	},
	{
		"Pname": "ASUNCION",
		"Pcode": "PYASU"
	},
	{
		"Pname": "DOHA",
		"Pcode": "QADOH"
	},
	{
		"Pname": "CONSTANTA",
		"Pcode": "ROCND"
	},
	{
		"Pname": "VICSANI",
		"Pcode": "ROVIS"
	},
	{
		"Pname": "FISHERYPORT",
		"Pcode": "RUVFP"
	},
	{
		"Pname": "KORSAKOV",
		"Pcode": "RUKOR"
	},
	{
		"Pname": "NAKHODKA",
		"Pcode": "RUNJK"
	},
	{
		"Pname": "NOVOROSSIYSK",
		"Pcode": "RUNVS"
	},
	{
		"Pname": "PEVEK",
		"Pcode": "RUPWE"
	},
	{
		"Pname": "ST. PETERSBURG",
		"Pcode": "RULED"
	},
	{
		"Pname": "VLIDIVOSTOK",
		"Pcode": "RUVVO"
	},
	{
		"Pname": "VOSTOCHNY",
		"Pcode": "RUVYP"
	},
	{
		"Pname": "VST",
		"Pcode": "RUVST"
	},
	{
		"Pname": "DAMMAN",
		"Pcode": "SADMN"
	},
	{
		"Pname": "DAMMAN",
		"Pcode": "SADMM"
	},
	{
		"Pname": "FUJAIRAH",
		"Pcode": "SAFUJ"
	},
	{
		"Pname": "GIZAN",
		"Pcode": "SAGIZ"
	},
	{
		"Pname": "JEDDAH",
		"Pcode": "SAJED"
	},
	{
		"Pname": "JUBAIL",
		"Pcode": "SAJUB"
	},
	{
		"Pname": "RIYADH",
		"Pcode": "SARIY"
	},
	{
		"Pname": "YANBU",
		"Pcode": "SAYNB"
	},
	{
		"Pname": "HONIARA",
		"Pcode": "SBHIR"
	},
	{
		"Pname": "MAHE",
		"Pcode": "SCHAW"
	},
	{
		"Pname": "PORT SUDAN",
		"Pcode": "SDPZU"
	},
	{
		"Pname": "GOTHERBURG",
		"Pcode": "SEGOT"
	},
	{
		"Pname": "MALMO",
		"Pcode": "SEMMA"
	},
	{
		"Pname": "SEOOL",
		"Pcode": "SEnull"
	},
	{
		"Pname": "STOCKHOLM",
		"Pcode": "SESTO"
	},
	{
		"Pname": "JURONG",
		"Pcode": "SGJUR"
	},
	{
		"Pname": "SINGAPORE",
		"Pcode": "SGSI3"
	},
	{
		"Pname": "SINGAPORE",
		"Pcode": "SGSG3"
	},
	{
		"Pname": "SINGAPORE",
		"Pcode": "SGSI1"
	},
	{
		"Pname": "SINGAPORE",
		"Pcode": "SGSIN"
	},
	{
		"Pname": "SINGAPORE",
		"Pcode": "SGSI4"
	},
	{
		"Pname": "SINGAPORE",
		"Pcode": "SGSI2"
	},
	{
		"Pname": "KOPER",
		"Pcode": "SIKOP"
	},
	{
		"Pname": "FREETOWN",
		"Pcode": "SLFNA"
	},
	{
		"Pname": "DAKAR",
		"Pcode": "SNDKR"
	},
	{
		"Pname": "KISMAYU",
		"Pcode": "SOKMU"
	},
	{
		"Pname": "MOGADISHU",
		"Pcode": "SOMGQ"
	},
	{
		"Pname": "SPACE",
		"Pcode": "SPACE"
	},
	{
		"Pname": "PARAMARIBO",
		"Pcode": "SRPBM"
	},
	{
		"Pname": "SAO TOME ISLAND",
		"Pcode": "STTMS"
	},
	{
		"Pname": "ODESSA",
		"Pcode": "SUDOS"
	},
	{
		"Pname": "ACAJUTLA",
		"Pcode": "SVAQJ"
	},
	{
		"Pname": "LOME",
		"Pcode": "TGLFW"
	},
	{
		"Pname": "BANGKOK",
		"Pcode": "THBKK"
	},
	{
		"Pname": "LEAMCHARBANG",
		"Pcode": "THLEM"
	},
	{
		"Pname": "Leam Char Bang",
		"Pcode": "THLCH"
	},
	{
		"Pname": "SATTAHIP",
		"Pcode": "THSAT"
	},
	{
		"Pname": "TUNIS",
		"Pcode": "TNTUN"
	},
	{
		"Pname": "NUKUALOFA",
		"Pcode": "TOTBU"
	},
	{
		"Pname": "AYT",
		"Pcode": "TRAYT"
	},
	{
		"Pname": "DERINCE",
		"Pcode": "TRDRC"
	},
	{
		"Pname": "GEMLIK",
		"Pcode": "TRGEM"
	},
	{
		"Pname": "ISKENDERUN",
		"Pcode": "TRISK"
	},
	{
		"Pname": "ISTANBUL",
		"Pcode": "TRIST"
	},
	{
		"Pname": "IZMIR",
		"Pcode": "TRIZM"
	},
	{
		"Pname": "KUMPORT",
		"Pcode": "TRKPX"
	},
	{
		"Pname": "MERSIN",
		"Pcode": "TRMER"
	},
	{
		"Pname": "PORT OF SPAIN",
		"Pcode": "TTPOS"
	},
	{
		"Pname": "HUALIEN",
		"Pcode": "TWHUN"
	},
	{
		"Pname": "KAOHSIUNG",
		"Pcode": "TWKAO"
	},
	{
		"Pname": "KAOHSIUNG",
		"Pcode": "TWKHH"
	},
	{
		"Pname": "KAOHSIUNG",
		"Pcode": "TWKHG"
	},
	{
		"Pname": "KAOHSIUNG(OOCL)",
		"Pcode": "TWKHS"
	},
	{
		"Pname": "KAOHSIUNG1",
		"Pcode": "TWKH1"
	},
	{
		"Pname": "KAOHSIUNG1",
		"Pcode": "TWKA1"
	},
	{
		"Pname": "KEELUNG",
		"Pcode": "TWKEL"
	},
	{
		"Pname": "KEELUNG1",
		"Pcode": "TWKEG"
	},
	{
		"Pname": "KHH NO.2",
		"Pcode": "TWKA2"
	},
	{
		"Pname": "OTHER-PORT",
		"Pcode": "TWZZZ"
	},
	{
		"Pname": "SUAO",
		"Pcode": "TWSUO"
	},
	{
		"Pname": "TACHUNG",
		"Pcode": "TWTCG"
	},
	{
		"Pname": "TAICHUNG",
		"Pcode": "TWTXJ"
	},
	{
		"Pname": "TAICHUNG",
		"Pcode": "TWTAI"
	},
	{
		"Pname": "TAIPAI",
		"Pcode": "TWTPE"
	},
	{
		"Pname": "TAITUNG",
		"Pcode": "TWTTT"
	},
	{
		"Pname": "DAR ES SALAAM",
		"Pcode": "TZDAR"
	},
	{
		"Pname": "TANGA",
		"Pcode": "TZTGT"
	},
	{
		"Pname": "ZANZIBAR",
		"Pcode": "TZZNZ"
	},
	{
		"Pname": "ILLYCHEVSK",
		"Pcode": "UAILK"
	},
	{
		"Pname": "ODESSA",
		"Pcode": "UAODS"
	},
	{
		"Pname": "ANCHORAGE",
		"Pcode": "USANC"
	},
	{
		"Pname": "BALTIMORE",
		"Pcode": "USBAL"
	},
	{
		"Pname": "BBLS",
		"Pcode": "USBBL"
	},
	{
		"Pname": "BEAUMONT",
		"Pcode": "USBPT"
	},
	{
		"Pname": "BOSTON",
		"Pcode": "USBOS"
	},
	{
		"Pname": "CAMDEN",
		"Pcode": "USCDE"
	},
	{
		"Pname": "CARB",
		"Pcode": "USCAR"
	},
	{
		"Pname": "CHARLESTON",
		"Pcode": "USCHS"
	},
	{
		"Pname": "CHICAGO",
		"Pcode": "USCHI"
	},
	{
		"Pname": "CL1",
		"Pcode": "USCL1"
	},
	{
		"Pname": "CL2",
		"Pcode": "USCL2"
	},
	{
		"Pname": "CL3",
		"Pcode": "USCL3"
	},
	{
		"Pname": "CL4",
		"Pcode": "USCL4"
	},
	{
		"Pname": "CL7",
		"Pcode": "USCL7"
	},
	{
		"Pname": "CL9",
		"Pcode": "USCL9"
	},
	{
		"Pname": "CSH",
		"Pcode": "USCSH"
	},
	{
		"Pname": "DALLAS",
		"Pcode": "USDFW"
	},

	{
		"Pname": "DETROIT",
		"Pcode": "USDET"
	},
	{
		"Pname": "DUTCH HARBOR",
		"Pcode": "USDUT"
	},
	{
		"Pname": "DUTCH HARBOR",
		"Pcode": "USDHC"
	},
	{
		"Pname": "ECSA",
		"Pcode": "USECS"
	},
	{
		"Pname": "ELIZABETH",
		"Pcode": "USELZ"
	},
	{
		"Pname": "EVERETT",
		"Pcode": "USPAE"
	},
	{
		"Pname": "FREEPORT",
		"Pcode": "USFRP"
	},
	{
		"Pname": "GALVESTON",
		"Pcode": "USGLS"
	},
	{
		"Pname": "GEORGETOWN",
		"Pcode": "USGGE"
	},
	{
		"Pname": "GULFPORT",
		"Pcode": "USGPT"
	},
	{
		"Pname": "HONOLULU",
		"Pcode": "USHNL"
	},
	{
		"Pname": "HOUSTON",
		"Pcode": "USHOU"
	},
	{
		"Pname": "HOUSTON1",
		"Pcode": "USHST"
	},
	{
		"Pname": "INLAND",
		"Pcode": "USIPI"
	},
	{
		"Pname": "IPDS",
		"Pcode": "USIPD"
	},
	{
		"Pname": "JACKSONVILLE",
		"Pcode": "USJAX"
	},
	{
		"Pname": "KSPAN",
		"Pcode": "USKSP"
	},
	{
		"Pname": "LAMS",
		"Pcode": "USLAM"
	},
	{
		"Pname": "LAR",
		"Pcode": "USLAR"
	},
	{
		"Pname": "LAT",
		"Pcode": "USLAT"
	},
	{
		"Pname": "LGB CHICAGO",
		"Pcode": "USLDC"
	},
	{
		"Pname": "LGB INLAND",
		"Pcode": "USLBI"
	},
	{
		"Pname": "LGB NEWYORK",
		"Pcode": "USLDN"
	},
	{
		"Pname": "LGBNORTH",
		"Pcode": "USLGN"
	},
	{
		"Pname": "LGBSOUTH",
		"Pcode": "USLGS"
	},
	{
		"Pname": "LONG BEACH 1",
		"Pcode": "USLGC"
	},
	{
		"Pname": "LONG BEACH 2",
		"Pcode": "USLGT"
	},
	{
		"Pname": "LONG BEACH 3",
		"Pcode": "USLMW"
	},
	{
		"Pname": "LONG BEACH NORTH",
		"Pcode": "USLXN"
	},
	{
		"Pname": "LONG BEACH SOUTH",
		"Pcode": "USLXS"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USLG4"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USKXW"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USLG5"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USOSM"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USAFM"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USCLQ"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USKX1"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USKX0"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USYDO"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USKX2"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USLG1"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USLGB"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USLG2"
	},
	{
		"Pname": "LONGBEACH",
		"Pcode": "USLG3"
	},
	{
		"Pname": "LONGBEACH INLAND",
		"Pcode": "USLGI"
	},
	{
		"Pname": "LONGBEACHUS",
		"Pcode": "USSEX"
	},
	{
		"Pname": "LONGVIEW",
		"Pcode": "USLGV"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USKDO"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USLA6"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USLA5"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USLA4"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USLA3"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USLHT"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USLAX"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USLAC"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USLAL"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USKKI"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USLAS"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USLAN"
	},
	{
		"Pname": "LOSANGELES",
		"Pcode": "USLA2"
	},
	{
		"Pname": "LOSANGELESLOCAL",
		"Pcode": "USLA1"
	},
	{
		"Pname": "MBS OF LA",
		"Pcode": "USMBS"
	},
	{
		"Pname": "MEMPHIS",
		"Pcode": "USMEM"
	},
	{
		"Pname": "MIAMI",
		"Pcode": "USMIA"
	},
	{
		"Pname": "MICRO LAND BRIDGE",
		"Pcode": "USMCB"
	},
	{
		"Pname": "MIDLAND",
		"Pcode": "USMWS"
	},
	{
		"Pname": "MILWAUKEE",
		"Pcode": "USMKE"
	},
	{
		"Pname": "MINI LAND BRIDGE",
		"Pcode": "USMLB"
	},
	{
		"Pname": "MOBILE",
		"Pcode": "USMOB"
	},
	{
		"Pname": "NEW ORLEANS",
		"Pcode": "USNEW"
	},
	{
		"Pname": "NEW YORK",
		"Pcode": "USNYC"
	},
	{
		"Pname": "NEWARK",
		"Pcode": "USEWR"
	},
	{
		"Pname": "NEWPORT NEWS",
		"Pcode": "USNEN"
	},
	{
		"Pname": "NORFOLK",
		"Pcode": "USORF"
	},
	{
		"Pname": "OAKLAND",
		"Pcode": "USOAC"
	},
	{
		"Pname": "OAKLAND",
		"Pcode": "USOKM"
	},
	{
		"Pname": "OAKLAND",
		"Pcode": "USOAK"
	},
	{
		"Pname": "OAKLAND",
		"Pcode": "USOAT"
	},
	{
		"Pname": "OAKLAND",
		"Pcode": "USOKC"
	},
	{
		"Pname": "OAKLAND",
		"Pcode": "USOKD"
	},
	{
		"Pname": "OAKLAND",
		"Pcode": "USOAI"
	},
	{
		"Pname": "PALM BEACH",
		"Pcode": "USPAB"
	},
	{
		"Pname": "PHILADELPHIA",
		"Pcode": "USPHL"
	},
	{
		"Pname": "PORT EVERGLADES",
		"Pcode": "USPEF"
	},
	{
		"Pname": "PORTLAND",
		"Pcode": "USPDX"
	},
	{
		"Pname": "PORTLAND",
		"Pcode": "USYP1"
	},
	{
		"Pname": "PORTLAND",
		"Pcode": "USPWM"
	},
	{
		"Pname": "PORTSMOUTH",
		"Pcode": "USPSM"
	},
	{
		"Pname": "SAN FRANCISCO",
		"Pcode": "USSFO"
	},
	{
		"Pname": "SAN PEDRO",
		"Pcode": "USSPQ"
	},
	{
		"Pname": "SAN PEDRO5",
		"Pcode": "USSP5"
	},
	{
		"Pname": "SAN PEDRO6",
		"Pcode": "USSP6"
	},
	{
		"Pname": "SAVANNAH",
		"Pcode": "USSAV"
	},
	{
		"Pname": "SEACHI",
		"Pcode": "USSTC"
	},
	{
		"Pname": "SEAIND",
		"Pcode": "USSTI"
	},
	{
		"Pname": "SEATLLE5",
		"Pcode": "USSE5"
	},
	{
		"Pname": "SEATTLE",
		"Pcode": "USSEA"
	},
	{
		"Pname": "SEATTLE",
		"Pcode": "USSE3"
	},
	{
		"Pname": "SEATTLE",
		"Pcode": "USSE1"
	},
	{
		"Pname": "SEATTLE",
		"Pcode": "USSE2"
	},
	{
		"Pname": "SEATTLE CHICAGO",
		"Pcode": "USSEC"
	},
	{
		"Pname": "SEATTLE NEW YORK",
		"Pcode": "USSEK"
	},
	{
		"Pname": "SEATTLE4",
		"Pcode": "USSE4"
	},
	{
		"Pname": "SEATTLE6",
		"Pcode": "USSE6"
	},
	{
		"Pname": "ST LOUIS",
		"Pcode": "USSTL"
	},
	{
		"Pname": "STOCKTON",
		"Pcode": "USSCK"
	},
	{
		"Pname": "TACOMA",
		"Pcode": "USTI1"
	},
	{
		"Pname": "TACOMA",
		"Pcode": "USTI2"
	},
	{
		"Pname": "TACOMA",
		"Pcode": "USYT2"
	},
	{
		"Pname": "TACOMA",
		"Pcode": "USTW2"
	},
	{
		"Pname": "TACOMA",
		"Pcode": "USTCM"
	},
	{
		"Pname": "TACOMA",
		"Pcode": "USTW1"
	},
	{
		"Pname": "TACOMA",
		"Pcode": "USTW3"
	},
	{
		"Pname": "TACOMA",
		"Pcode": "USYT1"
	},
	{
		"Pname": "TACOMA",
		"Pcode": "USTIW"
	},
	{
		"Pname": "TAMPA",
		"Pcode": "USTPA"
	},
	{
		"Pname": "TCB OF TACOMA",
		"Pcode": "USTCB"
	},
	{
		"Pname": "TCD OF TACOMA",
		"Pcode": "USTCD"
	},
	{
		"Pname": "US GULF",
		"Pcode": "USGLF"
	},
	{
		"Pname": "US MIDDLE WEST",
		"Pcode": "USMIW"
	},
	{
		"Pname": "VANCOUVER",
		"Pcode": "USBCB"
	},
	{
		"Pname": "WCCA",
		"Pcode": "USWCC"
	},
	{
		"Pname": "WCSA",
		"Pcode": "USWCS"
	},
	{
		"Pname": "WILMINGTON",
		"Pcode": "USILM"
	},
	{
		"Pname": "WILMINGTON",
		"Pcode": "USWTN"
	},
	{
		"Pname": "WSCA",
		"Pcode": "USWSC"
	},
	{
		"Pname": "WSS",
		"Pcode": "USWSS"
	},
	{
		"Pname": "YYG",
		"Pcode": "USYYG"
	},
	{
		"Pname": "YYI",
		"Pcode": "USYYI"
	},
	{
		"Pname": "YYW",
		"Pcode": "USYYW"
	},

	{
		"Pname": "MONTEVIDEO",
		"Pcode": "UYMVD"
	},
	{
		"Pname": "ST VINCENT",
		"Pcode": "VCSVD"
	},
	{
		"Pname": "EL GUAMACHE",
		"Pcode": "VEEGU"
	},
	{
		"Pname": "LA GUAIRA",
		"Pcode": "VELAG"
	},
	{
		"Pname": "MARACAIBO",
		"Pcode": "VEMAR"
	},
	{
		"Pname": "MATANZAS",
		"Pcode": "VEMTV"
	},
	{
		"Pname": "PUERTO CABELLO",
		"Pcode": "VEPBL"
	},
	{
		"Pname": "DA NANG",
		"Pcode": "VNDAD"
	},
	{
		"Pname": "DA-NANG",
		"Pcode": "VNTOU"
	},
	{
		"Pname": "HAIPHONG",
		"Pcode": "VNHPH"
	},
	{
		"Pname": "HOCHIMINH",
		"Pcode": "VNSGN"
	},
	{
		"Pname": "HOCHIMINH",
		"Pcode": "VNSGC"
	},
	{
		"Pname": "HOCHIMINH",
		"Pcode": "VNVIC"
	},
	{
		"Pname": "HOCHIMINH",
		"Pcode": "VNCLP"
	},
	{
		"Pname": "HOCHIMINH",
		"Pcode": "VNHCM"
	},
	{
		"Pname": "VUNGTAU",
		"Pcode": "VNVUT"
	},
	{
		"Pname": "PORTVILA",
		"Pcode": "VUPVI"
	},
	{
		"Pname": "SANTO",
		"Pcode": "VUSAN"
	},
	{
		"Pname": "VILA",
		"Pcode": "VUVLI"
	},
	{
		"Pname": "APIA",
		"Pcode": "WSAPW"
	},

	{
		"Pname": "ADEN",
		"Pcode": "YEADE"
	},
	{
		"Pname": "HODEIDAH",
		"Pcode": "YEHOD"
	},
	{
		"Pname": "MINA QABOOS",
		"Pcode": "YEMNQ"
	},
	{
		"Pname": "DUBROVNIK",
		"Pcode": "YUDBV"
	},
	{
		"Pname": "PULA",
		"Pcode": "YUPUY"
	},
	{
		"Pname": "SPLIT",
		"Pcode": "YUSPU"
	},
	{
		"Pname": "ZADAR",
		"Pcode": "YUZAD"
	},
	{
		"Pname": "CAPETOWN",
		"Pcode": "ZACPT"
	},
	{
		"Pname": "DURBAN",
		"Pcode": "ZADUR"
	},
	{
		"Pname": "EAST LONDON",
		"Pcode": "ZAELS"
	},
	{
		"Pname": "MOSSEL BAY",
		"Pcode": "ZAMZY"
	},
	{
		"Pname": "NGQURA",
		"Pcode": "ZAZBA"
	},
	{
		"Pname": "PORT ELIZABETH",
		"Pcode": "ZAPLZ"
	},
	{
		"Pname": "MATADI",
		"Pcode": "ZRMAT"
	},

	{
		"Pname": "SHAHAM",
		"Pcode": "ZZP12"
	},
	{
		"Pname": "SHARTM",
		"Pcode": "ZZP11"
	},
	{
		"Pname": "SHASOU",
		"Pcode": "ZZP13"
	},
	{
		"Pname": "SMX",
		"Pcode": "ZZSMX"
	}

],

	};

	this.bookings = [];
}

function Booking() {
	// body...
	this.name = new Date() + "_Traxens_IFTMBC_ #1";
	this.created = new Date();
	this.modified = new Date();
	this.data = {
		 parentbookingnbr: '',
     //childbookingnbr: '',
	    intchnb: 1,
 	    sender: '',
		recipient: "TRAXENS",
		 category: '6',
		 bookingagent:({
			"Bookingpartners": '',
			"Partnercode": ''
			}),
		 carrier: ({
			"carriercode": '',
			"carriername": ''
			}),
		shipper: ({
			"Bookingpartners": '',
			"Partnercode": ''
			}),
		consignee: ({
			"Bookingpartners": '',
			"Partnercode": ''
			}),
		POO: ({
				"Code": '',
				"Nom": ''
				}),
		FPD: ({
				"Code": '',
				"Nom": ''
				}),
		POL: '',
		POD: '',
		POO_DDE: '',
		POL_COD:'',
		POD_ETA: '',
		FPD_DDF: '',
		POO_PUDF: '',
		POL_ETD: '',
		POD_ETD: '',
		FPD_PUDE: '',
		poo_vehiclname: '',
		pol_vesselname: '',
		fpd_vehiclname: '',
		poo_vehicleid: '',
		pol_vesselid: '',
		fpd_vehicleid: '',
		pol_firstvoyagereference:'',
		PTS1: ({
				"Pcode": '',
				"Pname": ''
				}),
		PTS2: ({
				"Pcode": '',
				"Pname": ''
				}),
		PTS3: ({
				"Pcode": '',
				"Pname": ''
				}),
		PTS4: ({
				"Pcode": '',
				"Pname": ''
				}),
		PTS1_ETD: '',
		PTS2_ETD: '',
		PTS3_ETD: '',
		PTS4_ETD: '',
		PTS1_voyagereference:'',
		PTS2_voyagereference:'',
		PTS3_voyagereference:'',
		PTS4_voyagereference:'',
		PTS1_vesselname:'',
		PTS2_vesselname:'',
		PTS3_vesselname:'',
		PTS4_vesselname:'',
		PTS1_vesselid: '',
		PTS2_vesselid: '',
		PTS3_vesselid: '',
		PTS4_vesselid: '',
		EQP: ({
			"Typecode": '',
			"Description": ''
		}),
		EQPlist: [],
		TransportTemp: 0,
		minTemp: 0,
		maxTemp: 0
	};
}

(function(angular){
	'use strict';
	var mainApp = angular.module("mainApp", ['AxelSoft']);

	mainApp.constant('APP_CONSTANTS', {
	    'APP_NAME': 'IFTMBC Generator',
	    'APP_VERSION': '1.0'
	});

    mainApp.factory('_', ['$window',
      function($window) {
        return $window._;
      }
    ]);


	mainApp.factory("fileReader", ['$q', function($q) {

	    var onLoad = function(reader, deferred, scope) {
	        return function() {
	            scope.$apply(function() {
	                deferred.resolve(reader.result);
	            });
	        };
	    };

	    var onError = function(reader, deferred, scope) {
	        return function() {
	            scope.$apply(function() {
	                deferred.reject(reader.result);
	            });
	        };
	    };

	    var onProgress = function(reader, scope) {
	        return function(event) {
	            scope.$broadcast("appProgress", {
	                total: event.total,
	                loaded: event.loaded
	            });
	        };
	    };

	    var getReader = function(deferred, scope) {
	        var reader = new FileReader();
	        reader.onload = onLoad(reader, deferred, scope);
	        reader.onerror = onError(reader, deferred, scope);
	        reader.onprogress = onProgress(reader, scope);
	        return reader;
	    };

	    var readAsDataURL = function(file, scope) {
	        var deferred = $q.defer();

	        var reader = getReader(deferred, scope);
	        reader.readAsDataURL(file);

	        return deferred.promise;
	    };

	    var readAsText = function(file, scope) {
	        var deferred = $q.defer();

	        var reader = getReader(deferred, scope);
	        reader.readAsText(file);

	        return deferred.promise;
	    };

	    return {
	        readAsDataUrl: readAsDataURL,
	        readAsText: readAsText
	    };
}]);


	mainApp.directive("ngFileSelect", ['$parse', 'fileReader', function($parse, fileReader) {
	    return {
	        link: function($scope, el, attrs) {
	            el.bind("change", function(e) {
	                var srcElement = (e.srcElement || e.target);
	                $scope.file = srcElement.files[0];
	                $scope.setProgressBar($scope.file.length, 0);
	                fileReader.readAsText($scope.file, $scope).then(function(result) {
	                    $parse(srcElement.attributes['ng-file-select'].value).assign($scope, result);
	                    srcElement.value = '';
	                    $scope.setProgressBar(0, 0);
	                });
	            });
	        }
	    };
	}]);

	mainApp.directive('focusMe', function($timeout) {
  return {
    scope: { trigger: '@focusMe' },
    link: function(scope, element) {
      scope.$watch('trigger', function(value) {
        if(value === "true") {
          $timeout(function() {
            element[0].focus();
          });
        }
      });
    }
  };
});

    mainApp.controller('MainController', [ '$rootScope', '$scope', '$filter', '$timeout', '$parse', 'fileReader', 'APP_CONSTANTS',
  	  function($rootScope, $scope, $filter, $timeout, $parse, fileReader, AppConstants) {

  	  	//models
        angular.extend($scope, {
            _: _,
            appName: AppConstants.APP_NAME,
            version: AppConstants.APP_VERSION,
            appProgress: {},
            appData: new AppData(),
			containers: '',
		    focus :false,
		    hideparentbookingnbr: true,
		    EQPlist: [],
			EQPserialnbrs: [],
		    bool : true,
			hideit : false,
			srcselecteddata : [],
		    dstselecteddata : [],
			s : 0,
			d : 0,
			EQPtypeR: true,
			BClist : false,
		    newIFTMBC: false,
			edit: false,
			first : false,
			second : false


        });

        //methods
        angular.extend($scope, {
            saveAppData: function() {
				
                if ($scope.objectUrl) {
                    window.URL.revokeObjectURL($scope.objectUrl);
                    $scope.objectUrl = null;
                }
                if ($scope.appData != null) {
                    var appData = angular.toJson($scope.appData);
                    var blob = new Blob([appData], { type: "application/json;charset=utf-8;" });
                    var downloadLink = angular.element('<a></a>');
                    $scope.objectUrl = window.URL.createObjectURL(blob);
                    downloadLink.attr('href', $scope.objectUrl);
                    downloadLink.attr('download', 'Created-Booking-data' +  $filter('date')(new Date(), 'yyMMdd') + '_' + $filter('date')(new Date(), 'HHmm') +  '.json');
                    downloadLink[0].click();
					
                } else {
                    alert("No data available!");
                }
            },

            loadAppData: function(data) {
                if (data == null || data == undefined || data == "") {
                    document.getElementById('app-data-fs1').click();
                    return;
                }

                try {
                    $scope.appData = angular.fromJson(data);
                    return true;
                } catch (e) {
                    alert("Invalid app data format");
                }
                return false;
            },

            editBooking: function(iBooking) {
                if (iBooking > 0 && iBooking >= $scope.appData.bookings.length)
                    return;
                $scope.bcmsg = $scope.appData.bookings[iBooking];
				$scope.bcmsg.bc =  $scope.bcmsg.data;
				
				if(localStorage.getItem('idd')){
				var  idd = parseInt(localStorage.getItem('idd')) + 1;
				localStorage.setItem('idd',idd);
					if(idd<10)
						 $scope.bcmsg.bc.intchnb = 'TXH00' + idd;
					else if(idd<100)
							 $scope.bcmsg.bc.intchnb = 'TXH0' + idd;
						else  $scope.bcmsg.bc.intchnb = 'TXH' + idd;
					}
				this.showandhide(1);
				this.parentbookingnbr();
				$scope.edit = true;
            },

            parentbookingnbr: function(){
            	if($scope.bcmsg.bc.category == 6 || $scope.bcmsg.bc.category == 1){
            		$scope.addB = true;
				//_.remove(this.bcmsg.bc, textrm => textrm.childbookingnbr);
			
				$scope.bcmsg.bc.childbookingnbr = '';
            		$scope.hideparentbookingnbr = true;
            		$scope.childBNrequired = false;
            		$scope.bcmsg.bc.childbookingnbr = '';
            		//this.booking.data.childbookingnbr = '';

            	}
            	if($scope.bcmsg.bc.category == 17 || $scope.bcmsg.bc.category == 54){
            	$scope.hideparentbookingnbr = false;
            	$scope.childBNrequired = true;
				//$scope.EQPlist = [];
            	$scope.bcmsg.bc.EQPlist = [];
				//	this.booking.data.EQPlist = [];
				}

        },

            deleteBooking: function(iBooking) {
                if (iBooking > 0 && iBooking >= $scope.appData.bookings.length)
                    return;
                $scope.appData.bookings.splice(iBooking, 1);
            },

            SwitchToAddBooking: function() {
				this.AddBooking();
               this.showandhide(1);
            },

            showandhide: function(id) {
            	if(id == 1){
            		$scope.BClist = !$scope.BClist;
            	$scope.newIFTMBC = !$scope.newIFTMBC;
            }
				if(id == 2){
				$scope.BClist = false;
            	$scope.newIFTMBC = false;	
					if(localStorage.getItem('idd')){
				var  idd = parseInt(localStorage.getItem('idd')) - 1;
				localStorage.setItem('idd',idd);
					if(idd<10)
						 $scope.bcmsg.bc.intchnb = 'TXH00' + idd;
					else if(idd<100)
							 $scope.bcmsg.bc.intchnb = 'TXH0' + idd;
						else  $scope.bcmsg.bc.intchnb = 'TXH' + idd;
					}
				}
          },

            setProgressBar: function(total, loaded, warning) {
                $timeout(function() {
                    $scope.$broadcast("appProgress", {
                        total: total,
                        loaded: loaded,
                        warning: warning
                    });
                });
            },

            AddBooking: function(){
				
			$scope.bcmsg = new Booking();
             $scope.bcmsg.bc = $scope.bcmsg.data; 
			 var  id = $scope.appData.bookings.length + 1;
			 var idd;
			 if(localStorage.getItem('idd')){
				  idd = parseInt(localStorage.getItem('idd')) + 1;
			}
			 else 
           		     idd = $scope.appData.bookings.length + 1;
					
					if(idd<10)
						 $scope.bcmsg.bc.intchnb = 'TXH00' + idd;
					else if(idd<100)
							 $scope.bcmsg.bc.intchnb = 'TXH0' + idd;
						else  $scope.bcmsg.bc.intchnb = 'TXH' + idd;
					localStorage.setItem('idd', idd);
				
            	  	$scope.bcmsg.name = $filter('date')(new Date(), 'yyMMdd') + '_' + $filter('date')(new Date(), 'HHmm') + '_Traxens_IFTMBC_ #' + id;
				  	//$scope.bcmsg.bc.EQPlist = [];
					$scope.myform.$setPristine();	
					//this.ExportEDI();
					$scope.EQPtypeR = true;
					$scope.EQPserialnbrs = [];
			},

			moveToBooking: function(item) {
				$scope.bcmsg.bc.EQPlist.push(item);
				$scope.EQPserialnbrs.splice($scope.EQPserialnbrs.indexOf(item), 1);
				
			  },
			  
	     	moveFromBooking: function(item) {
				$scope.EQPserialnbrs.push(item);
				$scope.bcmsg.bc.EQPlist.splice($scope.bcmsg.bc.EQPlist.indexOf(item), 1);   
			  },

			moveItem: function(sourceID) {

					if(sourceID == 's'){
							if($scope.EQPserialnbrs != '' || $scope.EQPserialnbrs != null){
										 angular.forEach($scope.EQPserialnbrs, function(item) {
					 $scope.bcmsg.bc.EQPlist.push(item);
			 });
			 $scope.EQPserialnbrs.length = 0;
							}
					}
						else if(sourceID == 'd'){
							if($scope.bcmsg.bc.EQPlist != '' || $scope.bcmsg.bc.EQPlist!= null){
										 angular.forEach($scope.bcmsg.bc.EQPlist, function(item) {
					 $scope.EQPserialnbrs.push(item);
			 });
			$scope.bcmsg.bc.EQPlist.length = 0;
							}
							
					}
						},

		 testseries: function() {
			 $scope.EQPserialnbrs = [];
				var contnbr = [];
				var ar = '';
				var ar = _.compact($scope.containers);
				var truelist = 0;

			
				for(var i=0;i<ar.length; i++){
							$scope.EQPserialnbrs[truelist] = ar[i];
							truelist++;
										 }

			$scope.EQPserialnbrs = $scope.EQPserialnbrs.map(function(item, i) {
										return {
											text: item,
											id: i
												}
									
			});
			
			$scope.hideit = !$scope.hideit;

			},

		SaveBooking: function() {
				//$scope.bcmsg.bc.EQPlist = $scope.EQPlist;
			if($scope.bcmsg != null && $scope.edit == false){
			$scope.appData.bookings.push($scope.bcmsg);
			}
			
			$scope.myform.$setPristine(true);
			$scope.myform.$setUntouched(true);
			$scope.edit = false;
		
				$scope.BClist = false;
				$scope.newIFTMBC = false;
				$scope.EQPserialnbrs = [];
				$scope.containersnbr = '';
		},

		ExportEDI: function()
				{
		
				    var msg = $scope.bcmsg.bc;
				    var interchange_date = $filter('date')(new Date(), 'yyMMdd');
					var interchange_time = $filter('date')(new Date(), 'HHmm');
					var interchange_number = msg.intchnb;
					var msgdatetime = interchange_date + interchange_time;

					var carriage_condition=0;
					if(msg.POO.Nom != '' && msg.FPD.Nom != '' && msg.POL != '' && msg.POD != '')
						  carriage_condition = 27;
					else  if(msg.FPD.Nom == '' && msg.POO.Nom != '' && msg.POL != '' && msg.POD != '')
						  		carriage_condition = 28;
					  		else  if(msg.POO.Nom == '' && msg.FPD.Nom != '' && msg.POL != '' && msg.POD != '')
								carriage_condition = 29;
					  			else  if(msg.POO.Nom == '' && msg.FPD.Nom == '' && msg.POL != '' && msg.POD != '')
						  			carriage_condition = 30;

					var IFTMBC = temp;

					var mapObj = {

						   		sender: 									msg.sender,
						   		recipient: 									msg.recipient,
								interchange_date: 							interchange_date,
								interchange_time: 							interchange_time,
								interchange_number: 						interchange_number,
								Pbooking_number:			 				msg.parentbookingnbr,
								message_function_code:						msg.category,
								message_date_time:							msgdatetime,
								carriage_condition:							carriage_condition,
								poo_locode:									msg.POO.Code,
								poo_name:									msg.POO.Nom,
								pol_locode:		     						msg.POL.Pcode,
								pol_name:									msg.POL.Pname,
								pol_etd:									msg.POL_ETD,
								carrier_code:								msg.carrier.carriercode,
								carrier_name:								msg.carrier.carriername,
								pol_voyage:									msg.pol_firstvoyagereference,
								pol_vessel_id:								msg.pol_vesselid,
								pol_vessel_name:							msg.pol_vesselname,
								pod_locode:									msg.POD.Pcode,
								pod_name:									msg.POD.Pname,
								pod_eta:									msg.POD_ETA,
								fpd_locode:									msg.FPD.Code,
								fpd_name:									msg.FPD.Nom,
								pts1_voyage:								msg.PTS1_voyagereference,
								pts2_voyage:								msg.PTS2_voyagereference,
								pts3_voyage:								msg.PTS3_voyagereference,
								pts4_voyage:								msg.PTS4_voyagereference,
								pts1_vessel_id:							  	msg.PTS1_vesselid,
								pts2_vessel_id:				   			    msg.PTS2_vesselid,
								pts3_vessel_id:							    msg.PTS3_vesselid,
								pts4_vessel_id:							    msg.PTS4_vesselid,
								pts1_vessel_name:						    msg.PTS1_vesselname,
								pts2_vessel_name:						    msg.PTS2_vesselname,
								pts3_vessel_name:						    msg.PTS3_vesselname,
								pts4_vessel_name:						    msg.PTS4_vesselname,
								pts1_locode:								msg.PTS1.Pcode,
								pts2_locode:								msg.PTS2.Pcode,
								pts3_locode:								msg.PTS3.Pcode,
								pts4_locode:								msg.PTS4.Pcode,
								pts1_name:									msg.PTS1.Pname,
								pts2_name:									msg.PTS2.Pname,
								pts3_name:									msg.PTS3.Pname,
								pts4_name:									msg.PTS4.Pname,
								pts1_etd: 									msg.PTS1_ETD,
								pts2_etd: 									msg.PTS2_ETD,
								pts3_etd: 									msg.PTS3_ETD,
								pts4_etd: 									msg.PTS4_ETD
					};

					var str = IFTMBC.replace(/\$/g, '');
					var rep = new RegExp(Object.keys(mapObj).join("|"),"gi")
					var Fmsg = str.replace(rep, function(matched){
  						return mapObj[matched];
						});
					var BookingAgentSegment = '';
					var BookingCarrier = '';
					var BookingShipper = '';
					var BookingConsignee = '';

						if(msg.bookingagent.Bookingpartners != ""){
							 BookingAgentSegment = "NAD+ZZZ+" + msg.bookingagent.Partnercode + ":160:ZZZ+" + msg.bookingagent.Bookingpartners + "'";

					 Fmsg = Fmsg + BookingAgentSegment;
					}
					if(msg.carrier.carriername != ""){
							 BookingCarrier = "NAD+CA+" + msg.carrier.carriercode + ":160:ZZZ+" + msg.carrier.carriername + "'";
				 Fmsg = Fmsg + BookingCarrier;
					}

					if(msg.shipper.Bookingpartners != ""){
							 BookingShipper = "NAD+CZ+" + msg.shipper.Partnercode + ":160:ZZZ+" + msg.shipper.Bookingpartners + "'";
					 Fmsg = Fmsg + BookingShipper;
					}
					if(msg.consignee.Bookingpartners != ""){
							 BookingConsignee = "NAD+CN+" + msg.consignee.Partnercode + ":160:ZZZ+" + msg.consignee.Bookingpartners + "'";
					 Fmsg = Fmsg + BookingConsignee;
					}


var equipmentslength = $scope.bcmsg.bc.EQPlist.length;
	if(equipmentslength != 0){
		var GDIs = '';
		var EQDs = '';
		for(var g=1;g<=equipmentslength;g++){
			var GDI = "GDI+" + g + "'" ;
			 GDIs+=GDI;
			 var typeR = msg.EQP.Typecode.substring(2,3);
			 if(typeR == 'R')
				 var EQD = "EQD+CN+" + $scope.bcmsg.bc.EQPlist[g-1].text + "+" + msg.EQP.Typecode + ":102:5'EQN+1:2'" + "TMP+2+" +  msg.TransportTemp + ":CEL'" + "RNG+5+CEL:" + msg.maxTemp  +":" + msg.minTemp + "'";
			else var EQD = "EQD+CN+" + $scope.bcmsg.bc.EQPlist[g-1].text + "+" + msg.EQP.Typecode + ":102:5'EQN+1:2'"; 
			EQDs+=EQD;
		}
var EQPs = '';
EQPs = GDIs + EQDs;
Fmsg = Fmsg + EQPs;
	}


				// segment number
			   var segment_count = 0;
			   var segnbsep = "'";
			   var wordarray = new Array();

			    	for(var j=0; j<Fmsg.length;j++){
			       if(Fmsg[j] == segnbsep)
			           segment_count++;
				}


var UNTUNZseg = "UNT+" + segment_count + "'UNZ+1+" + interchange_number + "'";

Fmsg = Fmsg + UNTUNZseg;
var FFmsg;
          var filename;

if(msg.category == 17 || msg.category == 54)
{

FFmsg = Fmsg.replace(/Cbooking_number/g, msg.childbookingnbr);
				 filename = 'TRAXENS_IFTMBC_' + interchange_date + interchange_time + msg.childbookingnbr + '.edi';
}
else   {FFmsg = Fmsg.replace(/Cbooking_number/g, msg.parentbookingnbr);
				 filename = 'TRAXENS_IFTMBC_' + interchange_date + interchange_time + msg.parentbookingnbr + '.edi';}


        			var blob = new Blob([FFmsg], {type: 'application/text;charset=utf-8;'});
									
					if (window.navigator && window.navigator.msSaveOrOpenBlob)
					     {
							window.navigator.msSaveOrOpenBlob(blob, filename);
							
						 } else {
							  if ($scope.objectUrl) {
                    window.URL.revokeObjectURL($scope.objectUrl);
                    $scope.objectUrl = null;
                }
							var e = document.createEvent('MouseEvents'),
							a = document.createElement('a');
							a.download = filename;
							a.href = window.URL.createObjectURL(blob);
							a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
							e.initEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
							a.dispatchEvent(e);
       							}
							
		$scope.myform.$setPristine(true);
        //$scope.myform.$setUntouched();
		//this.SaveBooking();
		if($('#btnexport').attr('id') == 'btnexport'){
        this.AddBooking();							
		this.parentbookingnbr();}
				
								$scope.EQPserialnbrs = [];
								$scope.containersnbr = '';
						//$scope.myform.$setPristine();
						//$scope.myform.$setUntouched();
	   },
    					 

        });

        $scope.$watch('appData1', function(newValue, oldValue, scope) {
            if (newValue != oldValue && newValue != undefined)
                $scope.loadAppData(newValue);
        });

        $scope.$on("appProgress", function(e, progress) {
            angular.extend($scope.appProgress, progress);
            angular.extend($scope.appProgress, {
                display: {
                    width: ((progress.loaded / progress.total) * 100) + '%'
                }
            });

            if (progress.warning)
                $scope.appProgress.display.backgroundColor = '#ff5600';
        });


        $timeout(function() {
            angular.extend($scope, {
                appName: AppConstants.APP_NAME,
                version: AppConstants.APP_VERSION
            });
        });

     	 $scope.$watch('containersnbr', function(newValue, oldValue){
				if(newValue != null) {
		 $scope.containers = newValue.split(/\n| /);}});
		    

		// date time picker format
		$('.form_datetime').datetimepicker({
					language:  'fr',
					autoclose: true,
					format: 'yyyymmddhhii',
					todayHighlight: 'true',
					todayBtn: true
			});


			if(localStorage.getItem('recipient')){
var a = localStorage.getItem('recipient');
			$scope.appData.settings.recipients = JSON.parse(a);}


$scope.testEQPtype = {
	onSelect: function (item) {
		var R = item.Typecode.substring(2,3);
		if(R == 'R'){
			$scope.EQPtypeR = false;
		}
		else 
			$scope.EQPtypeR = true;
}
};
            $scope.growableCarriers = {
                        displayText: 'Select or Add a new Recipient...',
                        addText: 'Add new Recipient',
                        onAdd: function (text) {
							var sendername = text.split('|')[0];
							var sendercode = text.split('|')[1];
								if((sendercode != '' || sendercode != undefined) && (sendername == '' || sendername == undefined))
									alert('please enter name');
                             else {var newItem = {"sendername": sendername, "sendercode":sendercode};
                            $scope.appData.settings.recipients.push(newItem);
							
							localStorage.setItem("recipient", JSON.stringify($scope.appData.settings.recipients));
                            return newItem;
							 }
                        }
                    };


			$("#reset").click(function() {
						 $("input").val("");
						  $("select").val("");
				 });





//var cntnbrexpr = /^([A-Za-z]{4})(\d{7})$/g ;///^[a-z]{0,4}\d{7}$/g;


		}
			]);


})(window.angular);
